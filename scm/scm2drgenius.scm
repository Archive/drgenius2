;;#!/usr/bin/guile -s
;;!#

;;;;;;;;;;;;;;;;;;;;;
;;                 ;;  
;;  GLOBAL VALUES  ;;
;;                 ;;  
;;;;;;;;;;;;;;;;;;;;;

(define itemId 0)
;; Contains the defined objects:
;; (category (attributes) (specificArgs))
(define objects '())

(define :id "id")
(define :type "type")
(define :name "name")
(define :color "color")
(define :thickness "thickness")
(define :style "style")
(define :masked "masked")
(define :ref "ref")
(define :extra "extra")

;;;;;;;;;;;;;;;;;;;
;;               ;;  
;; XML DOCUMENT  ;;
;;               ;;  
;;;;;;;;;;;;;;;;;;;

(define (startDoc)
  (xml:out "<drgenius>")
  (newline))

(define (endDoc)
  (xml:out "</drgenius>")
  (newline))

(define (startFigure name)
  (xml:out "<drgeo name=\"")
  (xml:out name)
  (xml:out "\">")
  (newline))

(define (endFigure)
  (xml:out "</drgeo>")
  (newline))

;;
;; Process the objects record in SCM list 
;; into a Dr. Genius XML document
;;
(define (scm2xml objects)
  (cond ((null? objects)
         #f)
        (else
         (item2xml (car objects))
         (scm2xml (cdr objects)))))
  
(define (item2xml item)
  (cond ((eq? (car item) 'point)
         (point2xml item))))

(define (point2xml item)
  (let ((type (getAttr ':type item)))
    (cond ((string=? type "Rotation")
           (xml:elem `(point ((parent () (:ref ,(specific1 item)))
			      (parent () (:ref ,(specific2 item)))
                              (parent () (:ref ,(specific3 item))))
                             ,(cadr item))))          
          ((string=? type "Symmetry")
           (xml:elem `(point ((parent () ,(:ref (specific1 item)))
			      (parent () ,(:ref (specific2 item))))
			     ,(cadr item))))
          ((string=? type "Reflexion")
           (xml:elem `(point ((parent () ,(:ref (specific1 item)))
			      (parent () ,(:ref (specific2 item))))
			     ,(cadr item))))		     
          ((string=? type "Translation")
           (xml:elem `(point ((parent () ,(:ref (specific1 item)))
			      (parent () ,(:ref (specicif2 item))))
			     ,(cadr item))))
          ((string=? type "Scale")
           (xml:elem `(point ((parent () (:ref ,(specific1 item)))
			      (parent () (:ref ,(specific2 item)))
			      (parent () (:ref ,(specific3 item))))
			     ,(cadr item))))
          ((string=? type "Coordinate")
           (xml:elem `(point ((parent () ,(:ref (specific1 item)))
			      (parent () ,(:ref (specific2 item))))
			     ,(cadr item))))
          ((string=? type "Free")
           (xml:elem `(point ((x ,(specific1 item) ())
                              (y ,(specific2 item) ()))
			     ,(cadr item))))
          
          ((string=? type "On_curve")
           (xml:elem `(point ((parent () (:ref ,(specific1 item)))
                              (value ,(specific2 item) ()))
                             ,(cadr item))))
          ((string=? type "Intersection")
           (xml:elem `(point ((parent () (:ref ,(specific1 item)))
			      (parent () (:ref ,(specific2 item))))
			     ,(cadr item))))
          ((string=? type "Middle_segment")
           (xml:elem `(point ((parent () (:ref ,(specific1 item))))
			     ,(cadr item))))
          ((string=? type "Middle_2pts")
           (xml:elem `(point ((parent () (:ref ,(specific1 item)))
			      (parent () (:ref ,(specific2 item))))
			     ,(cadr item)))))))

              
;;;;;;;;;;;;;;;;;
;;             ;;  
;;   HELPERS   ;;
;;             ;;  
;;;;;;;;;;;;;;;;;
;;
;; ITEM HELPERS
;;
(define (specific1 item)
  (car (list-ref item 2)))

(define (specific2 item)
  (cadr (list-ref item 2)))

(define (specific3 item)
  (caddr (list-ref item 2)))

(define (assocItem id objects)
  (cond ((null? objects)
         #f)
        (else
         (if (eqv? (cadr (member ':id (cadar objects))) id)
             (car objects)
             (assocItem id (cdr objects))))))
          
(define (getAttr attr item)
  (cadr (memq attr (cadr item))))


(define (xml:out this)
  (display this))

(define (newItem item)
  (let ((myId itemId))
    (set! objects 
          (append objects (list item)))
    (set! itemId (+ 1 itemId))
    myId))
  

;;
;; attr is 
;; (attrName attrValue ...)
;;
(define (xml:@ attr)
  (cond ((not (null? attr))
         (xml:out (eval (car attr)))
         (xml:out "=\"")
         (xml:out (cadr attr))
         (xml:out "\" ")
         (xml:@ (cddr attr)))))
 
;;
;; value is a triplet:
;; elementName - elementValue - elementAttributes
;; (note: elementValue can be nested)
(define (xml:elem value)
  (xml:out "<")
  (xml:out (car value))
  (xml:out " ")
  (xml:@ (caddr value))
  (xml:out ">")
  (if (list? (cadr value))
      (for-each xml:elem (cadr value))
      (xml:out (cadr value)))
  (xml:out "</")
  (xml:out (car value))
  (xml:out ">"))



;;;;;;;;;;;;;;;;;
;;             ;;  
;;    POINT    ;;
;;             ;;  
;;;;;;;;;;;;;;;;;

;;
;; Free point on the plane
;;
(define (point x y . optional)
  (newItem (list 'point                    
                 (append optional (list ':id itemId ':type "Free"))
                 (list x y))))

;;
;; Free point on a line
;;
(define (pointOnLine line x . optional)
  (newItem (list 'point
                 (append optional (list ':id itemId ':type "On_curve"))
                 (list line x))))

;;
;; Intersection point
;; k = 0 | 1
;;
(define (intersection curve1 curve2 k . optional)
  (newItem (list 'point
                 (append optional (list ':id itemId ':type "Intersection" ':extra k))
                 (list curve1 curve2))))

;;
;; Middle segment
;;
(define (milieuSegment segment . optional)
  (newItem (list 'point
                 (append optional (list ':id itemId ':type "Middle_segment"))
                 (list segment))))

;;
;; Middle two points
;;
(define (middle A B . optional)
  (newItem (list 'point
                 (append optional (list ':id itemId ':type "Middle_2pts"))
                 (list A B))))

;;
;; Point defined by its coordinate
;;
(define (pointxy x y . optional)
  (newItem (list 'point
                 (append optional (list ':id itemId ':type "Coordinate"))
                 (list x y))))
                                  
         
;;;;;;;;;;;;;;;;;
;;             ;;  
;;    LINE     ;;
;;             ;;  
;;;;;;;;;;;;;;;;;

;;
;; Line by two points 
;;
(define (line a b . optional)
  (newItem (list 'line
                 (append optional (list ':id itemId ':type "2pts"))
                 (list a b))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                       ;;  
;;    TRANSFORMATION     ;;
;;                       ;;  
;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (rotation item center radius . optional)
    (let ((type (car (assocItem item objects))))
      (newItem (list type
                     (append optional (list ':id itemId ':type "Rotation"))
                     (list item center radius)))))
       
(define (symmetry item center . optional)
  (let ((type (car (assocItem item objects))))
    (newItem (list type
		   (append optional (list ':id itemId ':type "Symmetry"))
		   (list item center)))))

(define (reflexion item line . optional)
  (let ((type (car (assocItem item objects))))
    (newItem (list type
		   (append optional (list ':id itemId ':type "Reflexion"))
		   (list item line)))))

(define (translation item vector . optional)
  (let ((type (car (assocItem item objects))))
    (newItem (list type
		   (append optional (list ':id itemId ':type "Translation"))
		   (list item vector)))))

(define (scale item center factor . optional)
  (let ((type (car (assocItem item objects))))
    (newItem (list type
		   (append optional (list ':id itemId ':type "Scale"))
		   (list item center factor)))))

;;
;; DEFINE YOUR FIGURE HERE
;;



(define A (point (random 5) 2 ':name "A"))
(define B (point 2 1 ':color "Red" ':style "round" ':masked "True"))
(define r (rotation A B 55 ':name "M'"))
(define l (line A B ':color "Yellow"))
(rotation l B 55)
(pointOnLine l 1.2)



;; Produce a XML document from the SCM one
(scm2xml objects)