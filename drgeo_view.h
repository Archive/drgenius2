
#ifndef DRGEO_VIEW_H
#define DRGEO_VIEW_H

#include <gnome.h>
#include "xmlinclude.h"
#include "gobobjs/drgenius-mdi-child.h"

#ifdef __cplusplus
extern "C" {
#endif				/* __cplusplus */

// Helper functions to get the name of avalaible macro in the registry
	char *firstMacroName ();
	char *nextMacroName ();
	gboolean saveMacro (gchar * name, xmlNodePtr tree);
	gboolean loadMacro (xmlNodePtr macroXml);
	gboolean exportFigureToLatex (GnomeMDIChild * child, 
				      gchar *fileName);
	gboolean exportFigureToPostScript (GnomeMDIChild * child,
				      gchar *fileName);

#ifdef __cplusplus
}

#endif				/* __cplusplus */
#endif
