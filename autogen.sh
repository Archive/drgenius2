#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Dr.Genius"

(test -f $srcdir/configure.in \
  && test -d $srcdir/drgeo \
  && test -f $srcdir/drgeo/drgeo_point.h) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level Dr.Genius directory"
    exit 1
}

. $srcdir/macros/autogen.sh
