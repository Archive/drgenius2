/*
 *  Dr Genius interactive geometry software
 * (C) Copyright Hilaire Fernandes  2001
 * 
 * 
 * 
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef DRGENIUS_CONFIG_H
#define DRGENIUS_CONFIG_H

#include <gnome.h>

void loadUserPreferences ();
void saveUserPreferences ();
void updateDialogFromUserPreferences (GtkObject *d);
void updateUserPreferencesFromDialog (GtkObject *d);
void initPreferencesBox ();
/*
  Some cllbacks for the property box
*/
void on_propertyBox_changed (GtkWidget *widget, gpointer data);
void on_propertyBox_apply (GtkWidget *widget, gint page, gpointer data);


#endif /* DRGENIUS_CONFIG_H */



