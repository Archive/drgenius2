/*
 *  Dr Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes, Laurent Gauthier  1997-2000
 * hilaire@seul.org 
 * lolo@seul.org
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glade/glade.h>

#include "drgeo_gtkmacro.h"
#include "drgeo_gtkdrawable.h"
#include "drgeo_numeric.h"
#include "drgeo_command.h"

// Event handlers.

static gboolean
delete_event_cb (GtkWidget * widget, GdkEventAny * event,
		 drgeoGtkMacroBuildDialog * dialog)
{
	dialog->hide ();
	dialog->clear ();
	return true;
}

static void
cancel_event_cb (GtkWidget * widget, drgeoGtkMacroBuildDialog * dialog)
{
	dialog->handleCancel ();
}

static void
finish_event_cb (GtkWidget * widget, gpointer druidpage,
		 drgeoGtkMacroBuildDialog * dialog)
{
	dialog->handleFinish ();
}

static gboolean
next_event_cb (GnomeDruidPage * page, GnomeDruid * druid,
	       drgeoGtkMacroBuildDialog * dialog)
{
	dialog->handleNext (page);
	return false;
}

static gboolean
back_event_cb (GnomeDruidPage * page, GnomeDruid * druid,
	       drgeoGtkMacroBuildDialog * dialog)
{
	dialog->handleBack (page);
	return false;
}


void drgeoGtkMacroBuildDialog::
show ()
{
	gtk_widget_show (dialog);
}

void drgeoGtkMacroBuildDialog::
hide ()
{
	gtk_widget_hide (dialog);
}

void drgeoGtkMacroBuildDialog::
clear ()
{
	// Clear the input and output lists.
	gtk_clist_clear (GTK_CLIST (input));
	gtk_clist_clear (GTK_CLIST (output));

	// Clear the description field.
	gtk_text_set_point (GTK_TEXT (text), 0);
	gtk_text_forward_delete (GTK_TEXT (text),
				 gtk_text_get_length (GTK_TEXT (text)));

	// Clear the macro name field.
	gtk_entry_set_text (GTK_ENTRY (entry), "");

	// Reset the builder.
	builder->clearOutput ();
	builder->clearInput ();
	builder->setName (NULL);
	builder->setDescription (NULL);
	// XXX dirty, this prevent a crash when user close the dialog
	// and still select items
	builder->setMode (drgeoMacroDescriptionMode);
}

void drgeoGtkMacroBuildDialog::
add (geometricObject * item)
{
	gchar *array[2];
	gint row;

	// Add the name of this item to the list of item selected.
	array[0] = item->getTypeName ();
	array[1] = NULL;

	switch (builder->getMode ()) {
	case drgeoMacroInputMode:
		builder->addInput (item);
		row = gtk_clist_find_row_from_data (GTK_CLIST (input), (gpointer) item);
		if (row >= 0) {
			//item already selected, remove it from the list
			gtk_clist_remove (GTK_CLIST (input), row);
		} else {
			row = gtk_clist_append (GTK_CLIST (input), array);
			gtk_clist_set_row_data (GTK_CLIST (input), row, item);
		}
		break;
	case drgeoMacroOutputMode:
		builder->addOutput (item);
		row = gtk_clist_find_row_from_data (GTK_CLIST (output), (gpointer) item);
		if (row >= 0) {
			//item already selected, remove it from the list
			gtk_clist_remove (GTK_CLIST (output), row);
		} else {
			row = gtk_clist_append (GTK_CLIST (output), array);
			gtk_clist_set_row_data (GTK_CLIST (output), row, item);
		}
		break;
	default:
		// XXX This should not happen.
		break;
	}
}

void drgeoGtkMacroBuildDialog::
handleNext (GnomeDruidPage * druidPage)
{
	if (druidPage == startPage) {
		// we enter the input mode
		builder->setMode (drgeoMacroInputMode);
	} else if (druidPage == inputPage) {
		// we enter the output mode
		builder->setMode (drgeoMacroOutputMode);
	} else if (druidPage == outputPage) {
		// we enter the description mode
		builder->setMode (drgeoMacroDescriptionMode);
	} else if (druidPage == descriptionPage) {
		// we enter the finish mode
		builder->setMode (drgeoMacroFinishMode);
	} else {
		printf ("No default in drgeoGtkMacroBuildDialog::handleNext\n");
	}
}

void drgeoGtkMacroBuildDialog::
handleBack (GnomeDruidPage * druidPage)
{
	if (druidPage == inputPage) {
		// we enter the start mode
		builder->setMode (drgeoMacroStartMode);
	} else if (druidPage == outputPage) {
		// we enter the input mode
		builder->setMode (drgeoMacroInputMode);
	} else if (druidPage == descriptionPage) {
		// we enter the output mode
		builder->setMode (drgeoMacroOutputMode);
	} else if (druidPage == finishPage) {
		// we enter the description mode
		builder->setMode (drgeoMacroDescriptionMode);
	} else {
		printf ("No default in drgeoGtkMacroBuildDialog::handleBack\n");
	}
}

void drgeoGtkMacroBuildDialog::
handleFinish ()
{
	macro *aMacro;
	drgeoMacroRegistry *registry;

	// Invoke the builder to create the new macro.
	builder->setName (gtk_entry_get_text (GTK_ENTRY (entry)));
	builder->setDescription (gtk_editable_get_chars (GTK_EDITABLE (text),
							 0,
							 gtk_text_get_length
							 (GTK_TEXT (text))));
	if (builder->checkParameters ()) {
		// Create the new macro.
		aMacro = builder->createMacro ();

		// Register it.
	      registry = drgeoMacroRegistry::get ();
		registry->add (aMacro);

		// Clean everything.
		hide ();
		clear ();
	}
}

void drgeoGtkMacroBuildDialog::
handleCancel ()
{
	hide ();
	clear ();
}

drgeoGtkMacroBuildDialog::drgeoGtkMacroBuildDialog (drgeoMacroBuilder * builder)
{
	GladeXML *xmlDialogWidget;
	GtkWidget *widget;


	this->builder = builder;

	xmlDialogWidget = glade_xml_new (DRGENIUS_GLADEDIR "/drgeo.glade",
					 "buildMacroDialog");

	dialog = glade_xml_get_widget (xmlDialogWidget, "buildMacroDialog");
	input = glade_xml_get_widget (xmlDialogWidget, "input");
	output = glade_xml_get_widget (xmlDialogWidget, "output");
	entry = glade_xml_get_widget (xmlDialogWidget, "entry");
	text = glade_xml_get_widget (xmlDialogWidget, "text");

	// Connect the signals of the ...
	widget = glade_xml_get_widget (xmlDialogWidget, "buildMacroDruid");
	gtk_signal_connect (GTK_OBJECT (widget), "cancel",
			GTK_SIGNAL_FUNC (cancel_event_cb), (gpointer) this);
	// ...Start page
	startPage = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget, "buildMacroPageStart");
	gtk_signal_connect (GTK_OBJECT (startPage), "next",
			    GTK_SIGNAL_FUNC (next_event_cb),
			    (gpointer) this);
	// ...Input page
	inputPage = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget, "buildMacroPageInput");
	gtk_signal_connect (GTK_OBJECT (inputPage), "next",
			    GTK_SIGNAL_FUNC (next_event_cb),
			    (gpointer) this);
	gtk_signal_connect (GTK_OBJECT (inputPage), "back",
			    GTK_SIGNAL_FUNC (back_event_cb),
			    (gpointer) this);
	// ...Output page
	outputPage = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget, "buildMacroPageOutput");
	gtk_signal_connect (GTK_OBJECT (outputPage), "next",
			    GTK_SIGNAL_FUNC (next_event_cb),
			    (gpointer) this);
	gtk_signal_connect (GTK_OBJECT (outputPage), "back",
			    GTK_SIGNAL_FUNC (back_event_cb),
			    (gpointer) this);
	// ...Description page
	descriptionPage = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget, "buildMacroPageDescription");
	gtk_signal_connect (GTK_OBJECT (descriptionPage), "next",
			    GTK_SIGNAL_FUNC (next_event_cb),
			    (gpointer) this);
	gtk_signal_connect (GTK_OBJECT (descriptionPage), "back",
			    GTK_SIGNAL_FUNC (back_event_cb),
			    (gpointer) this);
	// ...Finish page
	finishPage = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget, "buildMacroPageFinish");
	gtk_signal_connect (GTK_OBJECT (finishPage), "back",
			    GTK_SIGNAL_FUNC (back_event_cb),
			    (gpointer) this);
	gtk_signal_connect (GTK_OBJECT (finishPage), "finish",
			    GTK_SIGNAL_FUNC (finish_event_cb),
			    (gpointer) this);

	gtk_signal_connect (GTK_OBJECT (dialog), "delete_event",
			    GTK_SIGNAL_FUNC (delete_event_cb), this);
}

drgeoGtkMacroBuildDialog::~drgeoGtkMacroBuildDialog ()
{
	gtk_widget_destroy (dialog);
}

// Event handlers.

static gboolean
play_delete_event_cb (GtkWidget * widget, GdkEventAny * event,
		      drgeoGtkMacroPlayDialog * dialog)
{
	dialog->handleFinish ();
	return true;
}

static void
play_finish_event_cb (GtkWidget * widget, gpointer druidpage,
		      drgeoGtkMacroPlayDialog * dialog)
{
	dialog->handleFinish ();
}
static void
play_cancel_event_cb (GtkWidget * widget, drgeoGtkMacroPlayDialog * dialog)
{
	dialog->handleFinish ();
}

static void
play_select_event_cb (GtkWidget * widget,
		      gint row,
		      gint column,
		      GdkEventButton * event,
		      drgeoGtkMacroPlayDialog * dialog)
{
	dialog->handleSelect (row, column);
}

static void
play_unselect_event_cb (GtkWidget * widget,
			gint row,
			gint column,
			GdkEventButton * event,
			drgeoGtkMacroPlayDialog * dialog)
{
	dialog->handleUnSelect ();
}

void drgeoGtkMacroPlayDialog::
handleSelect (int row, int column)
{
	// Get the name of the macro selected.
	gtk_clist_get_text (GTK_CLIST (list), row, column, &macroName);
	// Inform the player of the new macro
	player->setMacro (macroName);
	// make sure there is nothing left in the description buffer
	gtk_text_set_point (GTK_TEXT (description), 0);
	gtk_text_forward_delete (GTK_TEXT (description),
			      gtk_text_get_length (GTK_TEXT (description)));
	// Update the description buffer of the dialog
	gtk_text_insert (GTK_TEXT (description), NULL, NULL, NULL,
			 player->getDescription (), -1);
}

void drgeoGtkMacroPlayDialog::
handleUnSelect ()
{
	// update description text buffer in case the user change it
	player->setDescription (gtk_editable_get_chars (GTK_EDITABLE (description),
							0,
							gtk_text_get_length
						 (GTK_TEXT (description))));
	// Clear item selection
	player->setMacro (NULL);
	// Forget the selected macro
	macroName = NULL;
	// clear the description buffer of dialog
	gtk_text_set_point (GTK_TEXT (description), 0);
	gtk_text_forward_delete (GTK_TEXT (description),
			      gtk_text_get_length (GTK_TEXT (description)));
}

void drgeoGtkMacroPlayDialog::
show ()
{
	gtk_widget_show (dialog);
}

void drgeoGtkMacroPlayDialog::
hide ()
{
	gtk_widget_hide (dialog);
}

void drgeoGtkMacroPlayDialog::
handleFinish ()
{
	// update description text buffer in case the user change it
	player->setDescription (gtk_editable_get_chars (GTK_EDITABLE (description),
							0,
							gtk_text_get_length
							(GTK_TEXT (description))));
	gtk_clist_clear (GTK_CLIST (list));
	// clear the dialog description buffer
	gtk_text_set_point (GTK_TEXT (description), 0);
	gtk_text_forward_delete (GTK_TEXT (description),
			      gtk_text_get_length (GTK_TEXT (description)));
	// unselect the macro and the selected item
	player->setMacro (NULL);
	hide ();
}

void drgeoGtkMacroPlayDialog::
add (geometricObject * item)
{
	// only valid item come there
	player->add (item);
}

drgeoGtkMacroPlayDialog::drgeoGtkMacroPlayDialog (drgeoMacroPlayer * player)
{
	GladeXML *xmlDialogWidget;
	GtkWidget *widget;
	drgeoMacroRegistry *registry;
	macro *aMacro;
	char *array[2];
	int found;

	this->player = player;
	macroName = NULL;
	player->setMacro (NULL);
	xmlDialogWidget = glade_xml_new (DRGENIUS_GLADEDIR "/drgeo.glade",
					 "playMacroDialog");

	dialog = glade_xml_get_widget (xmlDialogWidget, "playMacroDialog");
	description = glade_xml_get_widget (xmlDialogWidget, "descriptionPlay");
	list = glade_xml_get_widget (xmlDialogWidget, "listPlay");
	druidListMacro = (GnomeDruidPage *) glade_xml_get_widget (xmlDialogWidget,
							  "druidListMacro");

	// Connect the signals
	gtk_signal_connect (GTK_OBJECT (dialog), "delete_event",
			    GTK_SIGNAL_FUNC (play_delete_event_cb), this);
	gtk_signal_connect (GTK_OBJECT (list),
			    "select_row",
			    GTK_SIGNAL_FUNC (play_select_event_cb),
			    this);
	gtk_signal_connect (GTK_OBJECT (list),
			    "unselect_row",
			    GTK_SIGNAL_FUNC (play_unselect_event_cb),
			    this);
	widget = glade_xml_get_widget (xmlDialogWidget, "playMacroDruid");
	gtk_signal_connect (GTK_OBJECT (widget), "cancel",
		   GTK_SIGNAL_FUNC (play_cancel_event_cb), (gpointer) this);
	widget = glade_xml_get_widget (xmlDialogWidget, "druidPageFinish");
	gtk_signal_connect (GTK_OBJECT (widget), "finish",
		   GTK_SIGNAL_FUNC (play_finish_event_cb), (gpointer) this);

	// Fill the list with the macro names.
      registry = drgeoMacroRegistry::get ();
	array[1] = NULL;
	found = 0;
	for (aMacro = registry->first ();
	     !registry->isDone ();
	     aMacro = registry->next ()) {

		// Append this macro name to the list.
		array[0] = aMacro->getName ();
		gtk_clist_append (GTK_CLIST (list), array);
		found = 1;
	}
	if (found) {
		gtk_clist_select_row (GTK_CLIST (list), 0, 0);
		gtk_clist_get_text (GTK_CLIST (list), 0, 0, &macroName);
		player->setMacro (macroName);
		gtk_text_set_point (GTK_TEXT (description), 0);
		gtk_text_forward_delete (GTK_TEXT (description),
			      gtk_text_get_length (GTK_TEXT (description)));
		gtk_text_insert (GTK_TEXT (description), NULL, NULL, NULL,
				 player->getDescription (), -1);
	}
}

drgeoGtkMacroPlayDialog::~drgeoGtkMacroPlayDialog ()
{
	gtk_widget_destroy (dialog);
}

