/*
 * Cha�nes de caract�res � traduire g�n�r�es par
 * Glade. Ajouter ce fichier au fichier POTFILE.in
 * de votre projet. NE PAS compiler ce fichier
 * avec le reste de votre application.
 */

gchar *s = N_("Build a macro-construction");
gchar *s = N_("Build a macro-construction");
gchar *s = N_("To build a macro construction,\n"
              "\n"
              "1. First select the input parameters,\n"
              "2. Next select the output parameters,\n"
              "3. Last chose a name and a description.");
gchar *s = N_("Select input parameters");
gchar *s = N_("Select output parameters");
gchar *s = N_("Choose name & description");
gchar *s = N_("Name");
gchar *s = N_("Enter the macro-construction name there");
gchar *s = N_("Macro Name");
gchar *s = N_("Description");
gchar *s = N_("Enter the macro-construction description there");
gchar *s = N_("Write down your description there.");
gchar *s = N_("Finish macro-construction");
gchar *s = N_("You've selected all the needed\n"
              "information to build a macro-construction.\n"
              "\n"
              "Press Finish to build it!\n"
              "\n"
              "Or you can change selection with the Back button.");
gchar *s = N_("Play a macro-construction");
gchar *s = N_("Play a macro-construction");
gchar *s = N_("To play a macro construction,\n"
              "\n"
              "1. First select a macro-construction from the list,\n"
              "2. Select items on the figure. Only relevant items \n"
              "to the selected macro-construction are selectable.\n"
              "Once enought items are selected, the macro is\n"
              "automaticly constructed.\n"
              "To start press the button \"next\".\n"
              "");
gchar *s = N_("Avalaible macro-construction");
gchar *s = N_("Macro-construction");
gchar *s = N_("Avalaible macro-construction");
gchar *s = N_("Description");
gchar *s = N_("You can edit the description");
gchar *s = N_("Tips");
gchar *s = N_("If you select another macro-construction,\n"
              "all the selected item will be unselected.\n"
              "\n"
              "You can edit the description text, your change\n"
              "will be recorded next time you save the macro.");
gchar *s = N_("Point");
gchar *s = N_("Point on an object or the background plane");
gchar *s = N_("The midpoint of a segment or between two points");
gchar *s = N_("The point(s) of intersection between two objects");
gchar *s = N_("Point given its coordinates");
gchar *s = N_("Create point");
gchar *s = N_("Create curve");
gchar *s = N_("Tools based on properties and transformations");
gchar *s = N_("Create numeric object");
gchar *s = N_("Macro-construction");
gchar *s = N_("Other");
gchar *s = N_("Select and move an object");
gchar *s = N_("200%");
gchar *s = N_("175%");
gchar *s = N_("150%");
gchar *s = N_("125%");
gchar *s = N_("100%");
gchar *s = N_("75%");
gchar *s = N_("50%");
gchar *s = N_("25%");
gchar *s = N_("Adjust the zoom factor");
gchar *s = N_("100%");
gchar *s = N_("Curve");
gchar *s = N_("Line defined by two points");
gchar *s = N_("Half-Line defined by two points, the first selected point is the origin");
gchar *s = N_("Segment defined by two points");
gchar *s = N_("Vector defined by two points");
gchar *s = N_("Circle defines by center and point, radius or segment");
gchar *s = N_("Arc circle defined by three points");
gchar *s = N_("Locus defined by a free point and a relative point");
gchar *s = N_("Polygon defined by n points. Last choosen point must be the initial point to terminate the selection");
gchar *s = N_("transformation");
gchar *s = N_("Line passing through one point and parallel to a line, half-line, etc.");
gchar *s = N_("Line passing through one point and orthogonal to a line, half-line, etc.");
gchar *s = N_("Axial symmetry of an object. When ambiguity, the first selected line is the line to transform");
gchar *s = N_("Central symmetry of an object. When ambiguity, the first selected point is the point to transform");
gchar *s = N_("Translation of an object. When ambiguity, the first selected vector is the vector to translate");
gchar *s = N_("Rotation of an object. When ambiguity, the first selected point is the point to rotate");
gchar *s = N_("Scale an object. When ambiguity, the first selected point is the point to transform");
gchar *s = N_("Numeric");
gchar *s = N_("Distance between objects, curve length, or edit free value");
gchar *s = N_("Angle defined by three points or two vectors");
gchar *s = N_("Vector and point coordinates, line and circle equation");
gchar *s = N_("Guile script using objects as input parameters");
gchar *s = N_("macro");
gchar *s = N_("Construct a macro");
gchar *s = N_("Execute pre-built macro");
gchar *s = N_("Guile script attached to a widget");
gchar *s = N_("other");
gchar *s = N_("Delete an object and its relatives");
gchar *s = N_("Change the style of an object");
gchar *s = N_("Change the property of an object");
gchar *s = N_("Free point properties");
gchar *s = N_("Abscissa");
gchar *s = N_("Ordinate");
gchar *s = N_("Calculatrice");
gchar *s = N_("Select Color");
gchar *s = N_("window2");
gchar *s = N_("Edit value");
gchar *s = N_("Custome User Interface");
gchar *s = N_("Close this dialog. Only the locked changes are set.");
gchar *s = N_("Lock the changes to the interface with a password");
gchar *s = N_("Lock");
gchar *s = N_("Unlock the changes to the interface (a password is required)");
gchar *s = N_("Unlock");
gchar *s = N_("Custome User Interface in the current view");
gchar *s = N_("Create point");
gchar *s = N_("Create curve");
gchar *s = N_("Tools based on properties and transformations");
gchar *s = N_("Create numeric object");
gchar *s = N_("Macro-construction");
gchar *s = N_("Other");
gchar *s = N_("Point on an object or the background plane");
gchar *s = N_("The midpoint of a segment or between two points");
gchar *s = N_("The point(s) of intersection between two objects");
gchar *s = N_("Point given its coordinates");
gchar *s = N_("Line defined by two points");
gchar *s = N_("Half-Line defined by two points, the first selected point is the origin");
gchar *s = N_("Segment defined by two points");
gchar *s = N_("Vector defined by two points");
gchar *s = N_("Circle defines by center and point, radius or segment");
gchar *s = N_("Arc circle defined by three points");
gchar *s = N_("Locus defined by a free point and a relative point");
gchar *s = N_("Polygon defined by n points. Last choosen point must be the initial point to terminate the selection");
gchar *s = N_("Line passing through one point and parallel to a line, half-line, etc.");
gchar *s = N_("Line passing through one point and orthogonal to a line, half-line, etc.");
gchar *s = N_("Axial symmetry of an object. When ambiguity, the first selected line is the line to transform");
gchar *s = N_("Central symmetry of an object. When ambiguity, the first selected point is the point to transform");
gchar *s = N_("Translation of an object. When ambiguity, the first selected vector is the vector to translate");
gchar *s = N_("Rotation of an object. When ambiguity, the first selected point is the point to rotate");
gchar *s = N_("Scale an object. When ambiguity, the first selected point is the point to transform");
gchar *s = N_("Distance between objects, curve length, or edit free value");
gchar *s = N_("Angle defined by three points or two vectors");
gchar *s = N_("Vector and point coordinates, line and circle equation");
gchar *s = N_("Guile script using objects as input parameters");
gchar *s = N_("Construct a macro");
gchar *s = N_("Execute pre-built macro");
gchar *s = N_("Guile script attached to a widget");
gchar *s = N_("Select and move an object");
gchar *s = N_("Delete an object and its relatives");
gchar *s = N_("Change the style of an object");
gchar *s = N_("Change the property of an object");
gchar *s = N_("Adjust the point style");
gchar *s = N_("Color");
gchar *s = N_("Shape");
gchar *s = N_("Size");
gchar *s = N_("Small");
gchar *s = N_("Normal");
gchar *s = N_("Large");
gchar *s = N_("Name");
gchar *s = N_("Visibility");
gchar *s = N_("Press this button to unmask the object");
gchar *s = N_("Not masked");
gchar *s = N_("Press this button to mask the object");
gchar *s = N_("Masked");
gchar *s = N_("Adjust the value style");
gchar *s = N_("Color");
gchar *s = N_("Style");
gchar *s = N_("Name");
gchar *s = N_("Visibility");
gchar *s = N_("Press this button to unmask the object");
gchar *s = N_("Not masked");
gchar *s = N_("Press this button to mask the object");
gchar *s = N_("Masked");
gchar *s = N_("Adjust the line style");
gchar *s = N_("Color");
gchar *s = N_("Name");
gchar *s = N_("Visibility");
gchar *s = N_("Press this button to unmask the object");
gchar *s = N_("Not masked");
gchar *s = N_("Press this button to mask the object");
gchar *s = N_("Masked");
gchar *s = N_("Edit the Dr. Genius Guile Script");
gchar *s = N_("Script");
