/*
 * Cha�nes de caract�res � traduire g�n�r�es par
 * Glade. Ajouter ce fichier au fichier POTFILE.in
 * de votre projet. NE PAS compiler ce fichier
 * avec le reste de votre application.
 */

gchar *s = N_("Save a Dr Genius session");
gchar *s = N_("Save selection");
gchar *s = N_("Save all");
gchar *s = N_("Select data to save");
gchar *s = N_("Data Type");
gchar *s = N_("Data Name");
gchar *s = N_("Defaut file name");
gchar *s = N_("The default file name used to save a session (several documents in one file)");
gchar *s = N_("Session:");
gchar *s = N_("Global preferences");
gchar *s = N_("Object Style");
gchar *s = N_("Color");
gchar *s = N_("Shape");
gchar *s = N_("Size");
gchar *s = N_("Small");
gchar *s = N_("Normal");
gchar *s = N_("Large");
gchar *s = N_("Point");
gchar *s = N_("Color");
gchar *s = N_("Style");
gchar *s = N_("Segment");
gchar *s = N_("Color");
gchar *s = N_("Style");
gchar *s = N_("Half-Line");
gchar *s = N_("Color");
gchar *s = N_("Style");
gchar *s = N_("Line");
gchar *s = N_("Color");
gchar *s = N_("Style");
gchar *s = N_("Vector");
gchar *s = N_("Color");
gchar *s = N_("Style");
gchar *s = N_("Circle");
gchar *s = N_("Color");
gchar *s = N_("Style");
gchar *s = N_("Arc-Circle");
gchar *s = N_("Color");
gchar *s = N_("Style");
gchar *s = N_("Locus");
gchar *s = N_("Color");
gchar *s = N_("Polygon");
gchar *s = N_("Color");
gchar *s = N_("Style");
gchar *s = N_("Angle");
gchar *s = N_("Color");
gchar *s = N_("Scalar");
gchar *s = N_("Default value");
gchar *s = N_("The number of undo/redo operations. 0 for infinite undo/redo level");
gchar *s = N_("Undo/Redo levels");
gchar *s = N_("Default figure name used when a new figure is created. This name is combined with a numeric indicator");
gchar *s = N_("Figure name");
gchar *s = N_("Defaut file name");
gchar *s = N_("Default file name used to export a geometric figure to the LaTeX format (pstricks extension)");
gchar *s = N_("LaTeX:");
gchar *s = N_("Default file name used to export a geometric figure to the encapsulated PostScript format");
gchar *s = N_("PostScript:");
gchar *s = N_("Default file name used to save a geometric figure");
gchar *s = N_("Geometric figure ");
gchar *s = N_("Geometric figure preferences");
gchar *s = N_("Select Color");
