/* Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes  1997-2000
 * hilaire@ofset.org 
 * 
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef drgeo_menu_h
#define drgeo_menu_h

#include <gnome.h>
#include "../gobobjs/drgeo-mdi-child.h"

#ifdef __cplusplus
extern "C" {
#endif				/* __cplusplus */
	
	void reconcile_popup_menu (DrGeniusMDIChild *child);
	GtkWidget **build_drgeo_popup_menu (void);
	void drgeo_install_popup_menu (GtkWidget * w, gpointer area);
	void on_menuBar_orientation_changed (GtkToolbar *bar, 
					     GtkOrientation orientation, 
					     gpointer data);
	void on_drgeoMenu_clicked (GtkWidget *w, gpointer data);
        gint on_subToolbar_clicked (GtkWidget *w, GdkEventButton *evt, gpointer data);
	void on_freePoint_clicked (GtkWidget *w, gpointer data);
	void on_middlePoint_clicked (GtkWidget *w, gpointer data);
	void on_intersection_clicked (GtkWidget *w, gpointer data);
	void on_coordinatesPoint_clicked (GtkWidget *w, gpointer data);
	void on_parallel_clicked (GtkWidget *w, gpointer data);
	void on_perpendicular_clicked (GtkWidget *w, gpointer data);
	void on_reflexion_clicked (GtkWidget *w, gpointer data);
	void on_symmetry_clicked (GtkWidget *w, gpointer data);
	void on_translation_clicked (GtkWidget *w, gpointer data);
	void on_rotation_clicked (GtkWidget *w, gpointer data);
	void on_scale_clicked (GtkWidget *w, gpointer data);
	void on_distance_clicked (GtkWidget *w, gpointer data);
	void on_angle_clicked (GtkWidget *w, gpointer data);
	void on_equation_clicked (GtkWidget *w, gpointer data);
	void on_script_clicked (GtkWidget *w, gpointer data);
	void on_buildMacro_clicked (GtkWidget *w, gpointer data);
	void on_runMacro_clicked (GtkWidget *w, gpointer data);
	void on_widgetScript (GtkWidget *w, gpointer);
	void on_line_clicked (GtkWidget *w, gpointer data);
	void on_halfLine_clicked (GtkWidget *w, gpointer data);
	void on_segment_clicked (GtkWidget *w, gpointer data);
	void on_vector_clicked (GtkWidget *w, gpointer data);
	void on_circle_clicked (GtkWidget *w, gpointer data);
	void on_arcCircle_clicked (GtkWidget *w, gpointer data);
	void on_locus_clicked (GtkWidget *w, gpointer data);
	void on_polygon_clicked (GtkWidget *w, gpointer data);
	void on_moveItem_clicked (GtkWidget *w, gpointer data);
	void on_styleItem_clicked (GtkWidget *w, gpointer data);
	void on_propertyItem_clicked (GtkWidget *w, gpointer data);
	void on_deleteItem_clicked (GtkWidget *w, gpointer data);
	void on_zoom_valeur_changed (GtkEditable *w, gpointer data);
#ifdef __cplusplus
}

#endif				/* __cplusplus */
#endif
