/* Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes  1997-1999
 * hilaire.fernandes@iname.com 
 * 
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gnome.h>
#include <string.h>
#include "drgeo_dialog.h"
#include "drgeo_drawable.h"
#include "drgeo_gtkdrawable.h"
#include "drgeo_numeric.h"


// To enter free value
static GtkWidget *drgeoEditDialog = NULL, *drgeoEntry;

// Used in the style dialod callback, I know it's ugly, but so easy
static drgeoFigure *selected_figure;

struct {
	drgeoPoint mouse;
	drgeoFigure *figure;
} drgeoDialogData;


static void drgeo_edit_dialog_cb (GtkWidget * dialog,
				  int button, gpointer data);

void
get_edited_value ()
{
	// build the drgeo Edit Dialog
	if (drgeoEditDialog != NULL && drgeoEditDialog->window != NULL) {
		gdk_window_show (drgeoEditDialog->window);
		gdk_window_raise (drgeoEditDialog->window);
		return;
	}
	drgeoEditDialog = gnome_dialog_new (N_ ("Enter a value"),
					    GNOME_STOCK_BUTTON_OK,
					    GNOME_STOCK_BUTTON_CANCEL,
					    NULL);

	gnome_dialog_set_default (GNOME_DIALOG (drgeoEditDialog), GNOME_OK);

	gnome_dialog_set_close (GNOME_DIALOG (drgeoEditDialog), TRUE);

	drgeoEntry = gnome_number_entry_new (NULL, N_ ("Value"));
	gnome_dialog_editable_enters (GNOME_DIALOG (drgeoEditDialog),
				  GTK_EDITABLE (gnome_number_entry_gtk_entry
					(GNOME_NUMBER_ENTRY (drgeoEntry))));

	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (drgeoEditDialog)->vbox),
			    drgeoEntry,
			    TRUE, TRUE, GNOME_PAD);

	gtk_signal_connect (GTK_OBJECT (drgeoEditDialog),
			    "clicked",
			    GTK_SIGNAL_FUNC (drgeo_edit_dialog_cb),
			    (gpointer) drgeoEntry);

	gtk_widget_show_all (drgeoEditDialog);

}


static void
drgeo_edit_dialog_cb (GtkWidget * dialog, int button, gpointer data)
{
	if (button == GNOME_OK) {
		geometricObject *item;
		double value;

		value = gnome_number_entry_get_number (GNOME_NUMBER_ENTRY (data));

		item = new numeric (drgeoDialogData.mouse, value, 
				    (numericType) FREE_VALUE, FALSE);
		(drgeoDialogData.figure)->createItem (item);
	}
}
