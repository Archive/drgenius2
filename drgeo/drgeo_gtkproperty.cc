/*
 *  Dr Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes  2001
 * hilaire@ofset.org 
 * 
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <glade/glade.h>

#include "drgeo_gtkproperty.h"
#include "drgeo_gtkdrawable.h"
#include "drgeo_numeric.h"
#include "drgeo_command.h"
#include "drgeo_point.h"
#include "drgeo_script.h"

extern GnomeHelpMenuEntry drgeniusHelp[];

// Callback for the free point property dialog
static void
on_freePointAbscissaEntry_activate (GtkWidget *widget, gpointer data)
{
	drgeoVector v(0,0);
	drgeoGtkPropertyDialog *dialog;
	point *item;
	gdouble val;

	val = strtod (gtk_entry_get_text (GTK_ENTRY(widget)), NULL);

	dialog = (drgeoGtkPropertyDialog *) data;
	item = (point *) dialog->getItem ();
	v.setX(val - item->getCoordinate().getX ());
	dialog->moveItem (v);	
}

static void
on_freePointOrdinateEntry_activate (GtkWidget *widget, gpointer data)
{
	drgeoVector v(0,0);
	drgeoGtkPropertyDialog *dialog;
	point *item;
	gdouble val;

	val = strtod (gtk_entry_get_text (GTK_ENTRY(widget)), NULL);

	dialog = (drgeoGtkPropertyDialog *) data;
	item = (point *) dialog->getItem ();
	v.setY(val - item->getCoordinate().getY ());
	dialog->moveItem (v);
}

static void movePointWithEntriesValues (GtkWidget *widget,
					drgeoGtkPropertyDialog *dialog)
{

	GtkEntry *abscissa, *ordinate;
	drgeoVector v;
	point *item;
	gdouble val1, val2;

	abscissa = (GtkEntry *) gtk_object_get_data (GTK_OBJECT (widget), 
						     "abscissaEntry");
	ordinate = (GtkEntry *) gtk_object_get_data (GTK_OBJECT (widget), 
						     "ordinateEntry");
	val1 = strtod (gtk_entry_get_text (abscissa), NULL);
	val2 = strtod (gtk_entry_get_text (ordinate), NULL);

	item = (point *) dialog->getItem ();
	v.set(val1 - item->getCoordinate().getX (),
	      val2 - item->getCoordinate().getY ());
	dialog->moveItem (v);
}

static void on_freePointButton_clicked(GtkWidget *widget, 
				       gint b, gpointer data)
{
	drgeoGtkPropertyDialog *dialog;
	dialog = (drgeoGtkPropertyDialog *) data;
	switch (b)
	{
	case 0:
		// Ok
		movePointWithEntriesValues (widget, dialog);
		dialog->hide();
		break;
	case 1:
		// Apply
		movePointWithEntriesValues (widget, dialog);
		break;
	case 2:
		// Cancel
		dialog->hide ();
		break;
	}
}

static gint on_freePointDialog_close (GtkWidget *widget, gpointer data)
{
	drgeoGtkPropertyDialog *dialog;
	dialog = (drgeoGtkPropertyDialog *) data;
	dialog->hide ();
	return TRUE;
}

// Callback for the edit value property dialog
static void
on_editValueEntry_activate (GtkWidget *widget, gpointer data)
{
	drgeoGtkPropertyDialog *dialog;
	gdouble val;

	val = strtod (gtk_entry_get_text (GTK_ENTRY(widget)), NULL);

	dialog = (drgeoGtkPropertyDialog *) data;
	dialog->setValue (val);
}

static void on_editValueButton_clicked(GtkWidget *widget, 
				       gint b, gpointer data)
{
	drgeoGtkPropertyDialog *drgeoDialog;
	drgeoDialog = (drgeoGtkPropertyDialog *) data;
	widget = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (widget), 
						   "valueEntry"));
	switch (b)
	{
	case 0:
		// Ok
		on_editValueEntry_activate (widget, drgeoDialog);
		drgeoDialog->hide();
		break;
	case 1:
		// Apply
		on_editValueEntry_activate (widget, drgeoDialog);
		break;
	case 2:
		// Cancel
		drgeoDialog->hide ();
		break;
	}
}

// When the user close the window containing the dialog
static gint on_propertyDialog_delete (GtkWidget *widget, GdkEventAny *e, gpointer data)
{
	drgeoGtkPropertyDialog *dialog;
	dialog = (drgeoGtkPropertyDialog *) data;
	dialog->hide ();
	return TRUE;
}

// Callback for the script edit dialog
static void updateScriptItemWithScriptDialog (GtkWidget *widget,
					      drgeoGtkPropertyDialog *dialog)
{
	GtkText *text;
	script *item;
	gchar *code;

	text = (GtkText *) gtk_object_get_data (GTK_OBJECT (widget), "script");
	code = gtk_editable_get_chars (GTK_EDITABLE (text), 0, 
				       gtk_text_get_length (text));
	dialog->setScript (code);
}

static gint on_scriptDialogOk (GtkWidget *widget, 
			       drgeoGtkPropertyDialog *drgeoDialog)
{

	updateScriptItemWithScriptDialog (widget, drgeoDialog);
	drgeoDialog->hide();
	return TRUE;
}

static gint on_scriptDialogApply (GtkWidget *widget, 
				  drgeoGtkPropertyDialog *drgeoDialog)
{
	updateScriptItemWithScriptDialog (widget, drgeoDialog);
	return TRUE;
}

static gint on_scriptDialogCancel (GtkWidget *widget, 
				  drgeoGtkPropertyDialog *drgeoDialog)
{
	drgeoDialog->hide ();	
	return TRUE;
}

drgeoGtkPropertyDialog::
drgeoGtkPropertyDialog (drgeoGtkDrawable *aDrawable)
{
	drawable = aDrawable;
	dialog = NULL;
	category = NO_OBJECT;
	type = NO_TYPE;
}

drgeoGtkPropertyDialog::
~drgeoGtkPropertyDialog ()
{
	hide ();
	if (dialog)
	  {
		  gtk_widget_destroy (dialog);
		  dialog = NULL;
	  }
}

void drgeoGtkPropertyDialog::
show ()
{
	if (dialog)
		gtk_widget_show_all (dialog);
}

void drgeoGtkPropertyDialog::
hide ()
{
	if (dialog)
	{
		gtk_widget_hide (dialog);
		/* unselected the selected item so they do no blink
		   anymore */
		drawable->figure->clearSelection ();		
	}
}

void drgeoGtkPropertyDialog::
edit (class geometricObject *aItem)
{
	GladeXML *xml;
	GtkWidget *widget, *widget2;
	drgeoPoint p;
	gchar out[256] = "";

	if (dialog && (aItem->getCategory () != category)
	    && (aItem->getType () != type))
		gtk_widget_destroy (dialog);

	// Acording to the object category, we build the corresponding dialog.
	switch (aItem->getCategory ())
	{
	case FREE_PT:
		
		if (category != aItem->getCategory ())
		{
			xml = glade_xml_new (DRGENIUS_GLADEDIR"/drgeo.glade",
					     "freePointPropertyDialog");
			//glade_xml_signal_autoconnect (xml);
			dialog = glade_xml_get_widget (xml, 
						       "freePointPropertyDialog");
			
			widget = glade_xml_get_widget (xml, "abscissaEntry");
			gtk_signal_connect (GTK_OBJECT(widget), "activate",
					    GTK_SIGNAL_FUNC(on_freePointAbscissaEntry_activate),
					    (gpointer) this);
			gtk_object_set_data (GTK_OBJECT (dialog), "abscissaEntry", widget);
			
			widget = glade_xml_get_widget (xml, "ordinateEntry");
			gtk_signal_connect (GTK_OBJECT(widget), "activate",
					    GTK_SIGNAL_FUNC(on_freePointOrdinateEntry_activate),
					    (gpointer) this);
			gtk_object_set_data (GTK_OBJECT (dialog), "ordinateEntry", widget);
			
			gtk_signal_connect (GTK_OBJECT (dialog), "clicked",
					    GTK_SIGNAL_FUNC(on_freePointButton_clicked),
					    (gpointer) this);	
			
			gtk_signal_connect (GTK_OBJECT (dialog), "close",
					    GTK_SIGNAL_FUNC(on_freePointDialog_close),
					    (gpointer) this);	
			
			gtk_object_destroy (GTK_OBJECT(xml));
		}
		p = ((point *) aItem)->getCoordinate ();
		// Update the entries field
		widget = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (dialog), 
							   "abscissaEntry");
		snprintf (out, 255, "%f", p.getX ());
		gtk_entry_set_text (GTK_ENTRY(widget), out);
		
		widget = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (dialog), 
							   "ordinateEntry");
		snprintf (out, 255, "%f", p.getY ());
		gtk_entry_set_text (GTK_ENTRY(widget), out);
		break;
	case NUMERIC:
		// Only free value can be edited
		if (aItem->getType () == FREE_VALUE)
		{
			if (category != aItem->getCategory ())
			{
				xml = glade_xml_new (
					DRGENIUS_GLADEDIR"/drgeo.glade",
					"editValuePropertyDialog");
				dialog = glade_xml_get_widget (
					xml, 
					"editValuePropertyDialog");
			
				widget = glade_xml_get_widget (xml, "valueEntry");
				gtk_signal_connect (GTK_OBJECT(widget), "activate",
						    GTK_SIGNAL_FUNC(on_editValueEntry_activate),
						    (gpointer) this);
				gtk_object_set_data (GTK_OBJECT (dialog), "valueEntry", widget);
			
				gtk_signal_connect (GTK_OBJECT (dialog), "clicked",
						    GTK_SIGNAL_FUNC(on_editValueButton_clicked),
						    (gpointer) this);	
			
				gtk_signal_connect (GTK_OBJECT (dialog), "close",
						    GTK_SIGNAL_FUNC(on_freePointDialog_close),
						    (gpointer) this);	
			
				gtk_object_destroy (GTK_OBJECT(xml));
			}
			// Update the entries field
			widget = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (dialog), 
								    "valueEntry");
			snprintf (out, 255, "%f", ((numeric *) aItem)->getValue ());
			gtk_entry_set_text (GTK_ENTRY(widget), out);
			break;
		}
		break;
	case SCRIPT:
		if (category != aItem->getCategory ())
		{
			// the corresponding dialog is not opened
			xml =  glade_xml_new (DRGENIUS_GLADEDIR"/drgeo.glade",
					      "scriptDialog");
			dialog = glade_xml_get_widget (xml, "scriptDialog");
			widget2 = glade_xml_get_widget (xml, "scriptText");
			gtk_object_set_data (GTK_OBJECT (dialog), "script", widget2);


			widget = glade_xml_get_widget (xml, "scriptDialogOk");
			gtk_object_set_data (GTK_OBJECT (widget), "script", widget2);
			gtk_signal_connect (GTK_OBJECT (widget), "clicked",
					    GTK_SIGNAL_FUNC(on_scriptDialogOk),
					    (gpointer) this);
	
			widget = glade_xml_get_widget (xml, "scriptDialogApply");
			gtk_object_set_data (GTK_OBJECT (widget), "script", widget2);
			gtk_signal_connect (GTK_OBJECT (widget), "clicked",
					    GTK_SIGNAL_FUNC(on_scriptDialogApply),
					    (gpointer) this);	

			widget = glade_xml_get_widget (xml, "scriptDialogCancel");
			gtk_signal_connect (GTK_OBJECT (widget), "clicked",
					    GTK_SIGNAL_FUNC(on_scriptDialogCancel),
					    (gpointer) this);
				
			widget = glade_xml_get_widget (xml, "scriptDialogHelp");
			gtk_signal_connect (GTK_OBJECT (widget), "clicked",
					    GTK_SIGNAL_FUNC(gnome_help_display),
					    (gpointer) &(drgeniusHelp[2]));

			gtk_signal_connect (GTK_OBJECT (dialog), "delete_event",
					    GTK_SIGNAL_FUNC(on_propertyDialog_delete),
					    (gpointer) this);

			
			gtk_object_destroy (GTK_OBJECT(xml));
		}
		// Update the text field with the script
		// First empty the scriptText dialog
		widget = (GtkWidget *) gtk_object_get_data (GTK_OBJECT (dialog),
							    "script");
		gtk_text_set_point (GTK_TEXT(widget), 0);
		gtk_text_forward_delete (GTK_TEXT(widget),
					 gtk_text_get_length (GTK_TEXT(widget)));
		gtk_text_insert (GTK_TEXT(widget), NULL, NULL, NULL,
				 (const gchar *) ((script *) aItem)->getScript (), -1);
		
		break;
	default:
		dialog = NULL;
		return;
	}
	// These are the characteristics of the item under edition:
	item = aItem;
	category = item->getCategory ();
	type = item->getType ();
	
	show ();
}

void drgeoGtkPropertyDialog::
moveItem (drgeoVector &v)
{
	drgeoVector t (0,0);

	drawable->getFigure()->moveItem (item, v);
	drawable->getFigure()->dragSelection (t, v);
	drawable->refresh ();
}

void drgeoGtkPropertyDialog::
setValue (gdouble value)
{
  gdouble *val;

  val = (gdouble *) g_malloc (sizeof (gdouble));
  *val = value;
  drawable->getFigure()->setItemAttribute (item, drgeoValue, 
					   (gpointer) val);
}

void drgeoGtkPropertyDialog::
setScript (gchar *script)
{
  drawable->getFigure()->setItemAttribute (item, drgeoScript, 
					   (gpointer) script);
}

void drgeoGtkPropertyDialog::
refresh ()
{
	drawable->refresh ();
}

geometricObject *drgeoGtkPropertyDialog::
getItem ()
{
	return item;
}


drgeoDrawable &drgeoGtkPropertyDialog::
getDrawable ()
{
	return (*(( drgeoDrawable *) drawable));
}
