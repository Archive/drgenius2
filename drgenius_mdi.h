
#ifndef DRGENIUS_MDI_H
#define DRGENIUS_MDI_H

#include <gnome.h>
#include "xmlinclude.h"

#ifdef __cplusplus
extern "C" {
#endif				/* __cplusplus */

	GnomeMDI *drgenius_mdi_new ();

	void drgenius_mdi_close (GnomeMDI * mdi);

/* close the current view */
	void drgenius_mdi_close_view (GnomeMDI * mdi);

/* open a session (multiple data view) */
	void drgenius_mdi_session_open (GnomeMDI * mdi, char *filename);
/* create a new mdi child */
	void drgenius_mdi_new_child (GnomeMDI * mdi, int childType, xmlNodePtr tree);

#ifdef __cplusplus
}

#endif				/* __cplusplus */
#endif
