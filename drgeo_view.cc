#include <config.h>
#include "drgeo_view.h"
#include "gobobjs/drgenius-mdi-child.h"

#include "drgeo_gtkdrawable.h"
#include "macro.h"

extern GnomeMDI *mdi;		// XXX ugly.


// Helper functions to get the name of avalaible macro in the registry
char *
firstMacroName ()
{
	drgeoMacroRegistry *registry;
	macro *mac;

      registry = drgeoMacroRegistry::get ();

	if ((mac = registry->first ()) == NULL)
		return NULL;
	return mac->getName ();
}
char *
nextMacroName ()
{
	drgeoMacroRegistry *registry;
	macro *mac;

      registry = drgeoMacroRegistry::get ();

	if ((mac = registry->next ()) == NULL)
		return NULL;
	return mac->getName ();
}

gboolean
saveMacro (gchar * name, xmlNodePtr tree)
{
	drgeoMacroRegistry *registry;

      registry = drgeoMacroRegistry::get ();
	return registry->save (name, tree);
}

gboolean 
loadMacro (xmlNodePtr macroXml)
{
	drgeoMacroRegistry *registry;

      registry = drgeoMacroRegistry::get ();
	return registry->load (macroXml);
}

gboolean
exportFigureToLatex (GnomeMDIChild * child, gchar *fileName)
{
	drgeoGtkDrawable *drawable;

	drawable = (drgeoGtkDrawable *)
		gtk_object_get_data (GTK_OBJECT (child), "drawable");
	if (drawable == NULL)
		return FALSE;
	return drawable->getFigure ()-> exportToLatex(fileName);
}

gboolean
exportFigureToPostScript (GnomeMDIChild * child, gchar *fileName)
{
	drgeoGtkDrawable *drawable;

	drawable = (drgeoGtkDrawable *)
		gtk_object_get_data (GTK_OBJECT (child), "drawable");
	if (drawable == NULL)
		return FALSE;
	return drawable->getFigure ()-> exportToPostScript(fileName);
}
