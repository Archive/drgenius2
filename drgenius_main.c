/*
 *  Dr Genius 
 * (C) Copyright FSF  2001
 * Author: hilaire@ofset.org 
 * 
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <locale.h>
#include <config.h>
#include <gnome.h>
#include <glade/glade.h>
#include <libgnorba/gnorba.h>
#include <guile/gh.h>

#include "drgenius_mdi.h"
#include "drgenius_config.h"
#include "drgeo_scm_interface.h"
#include "drgenius_help.h"

static void session_die(GnomeClient* client, gpointer client_data);

static gint save_session(GnomeClient *client, gint phase, 
                         GnomeSaveStyle save_style,
                         gint is_shutdown, GnomeInteractStyle interact_style,
                         gint is_fast, gpointer client_data);

void main_prog(int argc, char* argv[]);

static char* file_name  = NULL;

struct poptOption options[] = {
  { 
    "file",
    'f',
    POPT_ARG_STRING,
    &file_name,
    0,
    N_("File to load"),
    N_("filename")
  },
  {
    NULL,
    '\0',
    0,
    NULL,
    0,
    NULL,
    NULL
  }
};

GnomeMDI* mdi;

void 
main_prog(int argc, char* argv[])
{
  CORBA_ORB orb;
  CORBA_Environment ev;

  GnomeClient* client;
  
  //  printf("PACKAGE:%s \n PATH:%s\n\n",PACKAGE, GNOMELOCALEDIR);
  bindtextdomain(PACKAGE, GNOMELOCALEDIR);  
  textdomain(PACKAGE);

  CORBA_exception_init(&ev);
  orb = gnome_CORBA_init_with_popt_table(PACKAGE, VERSION, &argc, argv, 
					 options, 0, NULL,
					 GNORBA_INIT_SERVER_FUNC, &ev);  
  CORBA_exception_free(&ev);

  /* Init Glade stuff */
  glade_gnome_init ();
  
  /* Session Management */
  
  client = gnome_master_client ();
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
                      GTK_SIGNAL_FUNC (save_session), argv[0]);
  gtk_signal_connect (GTK_OBJECT (client), "die",
                      GTK_SIGNAL_FUNC (session_die), NULL);


  setlocale (LC_NUMERIC, "C");

  /* Load user preferences */
  loadUserPreferences ();

  /* declare drgeo interface in guile */
  gh_new_procedure ("move", drgeo_scm_move, 2, 0, 0);
  gh_new_procedure ("getAbscissa", drgeo_scm_getAbscissa, 1, 0, 0);
  gh_new_procedure ("setAbscissa", drgeo_scm_setAbscissa, 2, 0, 0);
  gh_new_procedure ("getCoordinates", drgeo_scm_getCoordinates, 1, 0, 0);
  gh_new_procedure ("setCoordinates", drgeo_scm_setCoordinates, 2, 0, 0);
  gh_new_procedure ("getSlope", drgeo_scm_getSlope, 1, 0, 0);
  gh_new_procedure ("getNorm", drgeo_scm_getNorm, 1, 0, 0);
  gh_new_procedure ("getLength", drgeo_scm_getLength, 1, 0, 0);
  gh_new_procedure ("getCenter", drgeo_scm_getCenter, 1, 0, 0);
  gh_new_procedure ("getRadius", drgeo_scm_getRadius, 1, 0, 0);
  gh_new_procedure ("getValue", drgeo_scm_getValue, 1, 0, 0);
  gh_new_procedure ("setValue", drgeo_scm_setValue, 2, 0, 0);


  /* Main app */

  mdi = drgenius_mdi_new(file_name);

  /* Display the main application.  */
  gnome_mdi_open_toplevel(mdi);

  if (file_name)
    drgenius_mdi_session_open (mdi, file_name);



  gtk_main();
}

int main(int argc, char* argv[])
{
	gh_enter(argc, argv, main_prog);
	g_print ("Can we quit?");
	return 0;
}

static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
              gint is_shutdown, GnomeInteractStyle interact_style,
              gint is_fast, gpointer client_data)
{
  gchar** argv;
  guint argc;

  /* allocate 0-filled, so it will be NULL-terminated */
  /* XXX: handle session management properly.  */
  argv = g_malloc0(sizeof(gchar*)*4);
  argc = 1;

  argv[0] = client_data;

  if (file_name)
    {
      argv[1] = "--file";
      argv[2] = file_name;
      argc = 3;
    }
  
  gnome_client_set_clone_command (client, argc, argv);
  gnome_client_set_restart_command (client, argc, argv);

  return TRUE;
}

static void
session_die(GnomeClient* client, gpointer client_data)
{
  gtk_main_quit();
}


