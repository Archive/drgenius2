/*
 *  Dr Genius interactive geometry software
 * (C) Copyright Laurent Gauthier,Hilaire Fernandes  1999-2000
 * 
 * 
 * 
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include "drgenius_menus.h"
#include <glade/glade.h>
#include "drgeo_view.h"
#include "drgenius_mdi.h"
#include "drgenius_config.h"
#include "drgeo_adaptDialog.h"

#include "icons/drgenius_new_figure.h"


extern gchar *globalSessionFileName;
extern gchar *drgeoFileName;
extern gchar *drgeoLatexFileName;
extern gchar *drgeoPostscriptFileName;

static gchar *sessionPathName = NULL;
static gchar *openPathName = NULL;

static void nothing_cb (GtkWidget * widget, gpointer data);
static void undo_cb (GtkWidget * widget, gpointer data);
static void redo_cb (GtkWidget * widget, gpointer data);
static void new_figure_cb (GtkWidget * widget, gpointer data);
static void new_editor_cb (GtkWidget * widget, gpointer data);
static void close_cb (GtkWidget * widget, gpointer data);
static void about_cb (GtkWidget * widget, gpointer data);
static void open_cb (GtkWidget * widget, gpointer data);
void save_cb (GtkWidget * widget, gpointer data); /* Used by canvas to trigger a save */
static void save_as_cb (GtkWidget * widget, gpointer data);
static void save_multiple_cb (GtkWidget * widget, gpointer data);
static void export_figure_to_latex_cb (GtkWidget * widget, gpointer data);
static void export_figure_to_postscript_cb (GtkWidget * widget, gpointer data);
static void save_session_ok_cb (GtkWidget * widget, GtkWidget *dialog);
static void export_figure_latex_ok_cb (GtkWidget * widget, GtkWidget *dialog);
static void export_figure_postscript_ok_cb (GtkWidget * widget, GtkWidget *dialog);
static void rename_view_cb (GtkWidget * widget, gpointer data);
static void rename_view_string_cb (gchar *reply, gpointer child);
static void customize_interface_cb (GtkWidget * widget, gpointer data);
static gboolean is_clist_row_selected (GtkCList * clist, gint row);

static GladeXML *xmlSessionWidget = NULL;
static xmlDocPtr drgeniusXml = NULL;

/*----- Property box ----------- */
static void preferences_box_cb(GtkWidget * widget, gpointer data);


static GnomeUIInfo new_menu[] =
{
	GNOMEUIINFO_ITEM (N_ ("_Figure"),
			  N_ ("Create a new geometric figure"),
			  new_figure_cb, drgenius_new_figure_h),
	GNOMEUIINFO_MENU_NEW_ITEM (N_ ("_Texte"),
				   N_ ("Create a new text"),
				   new_editor_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo export_menu[] =
{
	{
		GNOME_APP_UI_ITEM, N_ ("Export figure to Latex"),
		N_ ("Export current figure to Latex (using PSTrick package)"),
		export_figure_to_latex_cb, NULL, NULL, 0, 0
	},
	
	{
		GNOME_APP_UI_ITEM, N_ ("Export figure to PostScript"),
		N_ ("Export current figure to PostScript (eps)"),
		export_figure_to_postscript_cb, NULL, NULL, 0, 0
	},
	GNOMEUIINFO_END
} ;

static GnomeUIInfo file_menu[] =
{
	GNOMEUIINFO_SUBTREE (N_ ("_New..."),
			     new_menu),

	GNOMEUIINFO_MENU_OPEN_ITEM (open_cb, NULL),

	GNOMEUIINFO_MENU_SAVE_ITEM (save_cb, NULL),

	GNOMEUIINFO_MENU_SAVE_AS_ITEM (save_as_cb, NULL),

	{
		GNOME_APP_UI_ITEM, N_ ("Save _multiple"),
		N_ ("Save several data (figures, macro-constructions.) in one file"),
		(void *) save_multiple_cb, NULL, NULL, 0, 0, 's', GDK_CONTROL_MASK + GDK_SHIFT_MASK
	},

	GNOMEUIINFO_SUBTREE (N_ ("Export As..."),
			     export_menu),

	GNOMEUIINFO_SEPARATOR,

	GNOMEUIINFO_MENU_CLOSE_ITEM (close_cb, NULL),

	GNOMEUIINFO_MENU_EXIT_ITEM (exit_cb, NULL),

	GNOMEUIINFO_END
};


static GnomeUIInfo edit_menu[] =
{
	GNOMEUIINFO_MENU_UNDO_ITEM (undo_cb, NULL),
	GNOMEUIINFO_MENU_REDO_ITEM (redo_cb, NULL),	
        GNOMEUIINFO_MENU_PREFERENCES_ITEM (preferences_box_cb, NULL),
	{
		GNOME_APP_UI_ITEM, N_ ("Rename"),
		N_ ("Rename the current view"),
		rename_view_cb, NULL, NULL, 0, 0
	},
	{
		GNOME_APP_UI_ITEM, N_ ("Custome Interface"),
		N_ ("Change the User Interface of the current figure"),
		customize_interface_cb, NULL, NULL, 0, 0
	},
	
       	GNOMEUIINFO_END
};

static GnomeUIInfo windows_menu[] =
{
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] =
{
	GNOMEUIINFO_HELP ("drgenius"),

	GNOMEUIINFO_MENU_ABOUT_ITEM (about_cb, NULL),

	GNOMEUIINFO_END
};

static GnomeUIInfo menu[] =
{
	GNOMEUIINFO_MENU_FILE_TREE (file_menu),
	GNOMEUIINFO_MENU_EDIT_TREE (edit_menu),
	GNOMEUIINFO_MENU_WINDOWS_TREE (windows_menu),
	GNOMEUIINFO_MENU_HELP_TREE (help_menu),
	GNOMEUIINFO_END
};

static GnomeUIInfo toolbar[] =
{

	GNOMEUIINFO_ITEM (NULL, N_ ("Create a new geometric figure"), 
			  new_figure_cb, drgenius_new_figure_h),
	GNOMEUIINFO_ITEM_STOCK (NULL, N_ ("Create a new text"), 
				new_editor_cb, GNOME_STOCK_PIXMAP_NEW),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_STOCK (NULL, N_ ("Undo modification"), undo_cb, GNOME_STOCK_PIXMAP_UNDO),
	GNOMEUIINFO_ITEM_STOCK (NULL, N_ ("Redo change"), redo_cb, GNOME_STOCK_PIXMAP_REDO),

	GNOMEUIINFO_END
};


void
drgeo_install_menus_and_toolbar (GtkWidget * app)
{
	gnome_app_create_toolbar_with_data (GNOME_APP (app), toolbar, app);
	gnome_app_create_menus_with_data (GNOME_APP (app), menu, app);
	gnome_app_install_menu_hints (GNOME_APP (app), menu);
	reconcile_grayout_widget ();
}

static void
nothing_cb (GtkWidget * widget, gpointer data)
{
	GtkWidget *dialog;
	GtkWidget *app;

	app = (GtkWidget *) data;

	dialog = gnome_ok_dialog_parented (
		       _ ("This does nothing; it is only a demonstration."),
						  GTK_WINDOW (app));
}

extern GnomeMDI *mdi;

static void
undo_cb (GtkWidget * widget, gpointer data)
{
	DrGeniusMDIChild *child;

	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));
	drgenius_mdi_child_undo (child, 0);
}
static void
redo_cb (GtkWidget * widget, gpointer data)
{
	DrGeniusMDIChild *child;

	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));
	drgenius_mdi_child_redo (child, 0);
}


static void
open_ok_cb (GtkWidget * widget, GtkWidget * dialog)
{
	char *filename;


	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog));
	if (!filename || !g_file_exists (filename)) 
	{
		GtkWidget *dlg;
		char *s;

		s = g_strdup_printf (_ ("Can't open file '%s'!"), filename);
		/* make a new dialog with the file selection as parent */
		dlg = gnome_error_dialog_parented (s, GTK_WINDOW (dialog));
		gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		g_free (s);
		gtk_widget_show (dlg);
		return;
	}
	g_free (openPathName);
	openPathName = g_strdup (filename);

	filename = g_strdup (filename);
	gtk_widget_destroy (dialog);

	drgenius_mdi_session_open (mdi, filename);

	g_free (filename);
	reconcile_grayout_widget ();
}

static void
open_cb (GtkWidget * widget, gpointer data)
{
        GtkWidget *dialog;
        GtkWidget *app;

	app = (GtkWidget *) data;

	dialog = gtk_file_selection_new (_ ("Open..."));
	if (openPathName)
	  gtk_file_selection_set_filename( GTK_FILE_SELECTION(dialog), openPathName);
	
	gtk_window_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (app));
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (dialog)->cancel_button),
				   "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (dialog));
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dialog)->ok_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (open_ok_cb),
			    (gpointer) dialog);
	gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (dialog));
	gtk_widget_show (dialog);
}


/* takes the file dialog in the data field, and the reply has to be 0 */
static void
really_save_cb (int reply, gpointer data)
{
	DrGeniusMDIChild *child;
	GtkWidget *dialog = data;
	char *filename;

	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog));

	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));

	if (reply != 0 ||
	    !child) {
		return;
	}
	/* call the signal calling wrapper for "save" */
	if (!drgenius_mdi_child_save (child, filename)) {
		GtkWidget *dlg;
		char *s;

		s = g_strdup_printf (_ ("Error saving to '%s'!"), filename);
		/* make a new dialog with the file selection as parent */
		dlg = gnome_error_dialog_parented (s, GTK_WINDOW (dialog));
		gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		g_free (s);
		gtk_widget_show (dlg);
		return;
	}
	gtk_widget_destroy (dialog);
}

/* takes the file dialog in the data field, and the reply has to be 0 */
static void
really_save_session_cb (int reply, gpointer data)
{
	GtkWidget *dialog = data;
	char *filename;

	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog));

	if (reply != 0)
		return;

	/* save the XML tree in drgeniusXml */
	if (!xmlSaveFile (filename, drgeniusXml)) {
		GtkWidget *dlg;
		char *s;

		s = g_strdup_printf (_ ("Error saving to '%s'!"), filename);
		/* make a new dialog with the file selection as parent */
		dlg = gnome_error_dialog_parented (s, GTK_WINDOW (dialog));
		gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		g_free (s);
		gtk_widget_show (dlg);
		return;
	}
	xmlFreeDoc (drgeniusXml);
	drgeniusXml = NULL;
	gtk_widget_destroy (dialog);
}

static void
really_export_figure_latex_cb (int reply, gpointer data)
{
	DrGeniusMDIChild *child;
	GtkWidget *dialog = data;
	char *filename;

	if (reply != 0)
		return;

	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog));
	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));

	if (!exportFigureToLatex ((GnomeMDIChild *) child, filename)) {
		GtkWidget *dlg;
		char *s;

		s = g_strdup_printf (_ ("Error exporting to '%s'!"), filename);
		/* make a new dialog with the file selection as parent */
		dlg = gnome_error_dialog_parented (s, GTK_WINDOW (dialog));
		gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		g_free (s);
		gtk_widget_show (dlg);
		return;
	}
	gtk_widget_destroy (dialog);
}

static void
really_export_figure_postscript_cb (int reply, gpointer data)
{
	DrGeniusMDIChild *child;
	GtkWidget *dialog = data;
	char *filename;

	if (reply != 0)
		return;

	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog));
	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));

	if (!exportFigureToPostScript ((GnomeMDIChild *) child, filename)) {
		GtkWidget *dlg;
		char *s;

		s = g_strdup_printf (_ ("Error exporting to '%s'!"), filename);
		/* make a new dialog with the file selection as parent */
		dlg = gnome_error_dialog_parented (s, GTK_WINDOW (dialog));
		gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		g_free (s);
		gtk_widget_show (dlg);
		return;
	}
	gtk_widget_destroy (dialog);
}

static void
save_ok_cb (GtkWidget * widget, GtkWidget * dialog)
{
	char *filename;
	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog));
	if (!filename)
		return;
	if (g_file_exists (filename)) {
		/* file exists so ask first before writing */
		GtkWidget *dlg;
		char *s;

		s = g_strdup_printf (_("File '%s' exists!\nOverwrite?"), filename);
		dlg = gnome_question_dialog_parented (s,
						      really_save_cb,
						      dialog,
						      GTK_WINDOW (dialog));
		gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		gtk_widget_show (dlg);
		g_free (s);
	} else
		/* just call the really save callback to issue to save signal */
		really_save_cb (0, dialog);
}

static void
save_as_cb (GtkWidget * widget, gpointer data)
{
	GtkWidget *app = data;
	GtkWidget *dialog;
	DrGeniusMDIChild *child;
	if (!gnome_mdi_get_active_child (mdi))
		return;
	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));

	if (!child->supports_save)
		return;

	dialog = gtk_file_selection_new (_ ("Save As..."));
	if (child->filename)
		gtk_file_selection_set_filename (GTK_FILE_SELECTION (dialog),
						 child->filename);
	else 
		gtk_file_selection_set_filename (GTK_FILE_SELECTION (dialog),
						 drgeoFileName);
	gtk_window_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (app));
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (dialog)->cancel_button),
				   "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (dialog));
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dialog)->ok_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (save_ok_cb),
			    (gpointer) dialog);
	gtk_widget_show (dialog);
}

void
save_cb (GtkWidget * widget, gpointer data)
{
	DrGeniusMDIChild *child;
	if (!gnome_mdi_get_active_child (mdi))
		return;
	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));
	if (!child->supports_save)
		return;

	/* if we have a filename */
	if (child->filename)
		/* call the signal calling wrapper for "save" */
		drgenius_mdi_child_save (child, child->filename);
	else
		/* just call the above callback for "Save As...", we need
		   to pass the data as that's the app */
		save_as_cb (NULL, data);
}
static void 
save_session_ok_cb (GtkWidget * widget, GtkWidget *dialog)
{
	char *filename;

	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog));
	if (!filename)
		return;
	g_free (sessionPathName);
	sessionPathName = g_strdup (filename);
	if (g_file_exists (filename)) {
		/* file exists so ask first before writing */
		GtkWidget *dlg;
		char *s;

		s = g_strdup_printf (_ ("File '%s' exists!\nOverwrite?"), filename);
		dlg = gnome_question_dialog_parented (s,
						      really_save_session_cb,
						      dialog,
						      GTK_WINDOW (dialog));
		gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		gtk_widget_show (dlg);
		g_free (s);
	} else 
		really_save_session_cb (0, dialog);
}

static void 
export_figure_latex_ok_cb (GtkWidget * widget, GtkWidget *dialog)
{
	char *filename;
	
	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog));
	if (!filename)
		return;
	if (g_file_exists (filename)) {
		/* file exists so ask first before writing */
		GtkWidget *dlg;
		char *s;

		s = g_strdup_printf (_ ("File '%s' exists!\nOverwrite?"), filename);
		dlg = gnome_question_dialog_parented (s,
						      really_export_figure_latex_cb,
						      dialog,
						      GTK_WINDOW (dialog));
		gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		gtk_widget_show (dlg);
		g_free (s);
	} else 
		really_export_figure_latex_cb (0, dialog);
}

static void
export_figure_postscript_ok_cb (GtkWidget * widget, GtkWidget *dialog)
{
	char *filename;
	
	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (dialog));
	if (!filename)
		return;
	if (g_file_exists (filename)) {
		/* file exists so ask first before writing */
		GtkWidget *dlg;
		char *s;

		s = g_strdup_printf (_ ("File '%s' exists!\nOverwrite?"), filename);
		dlg = gnome_question_dialog_parented (s,
						      really_export_figure_postscript_cb,
						      dialog,
						      GTK_WINDOW (dialog));
		gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		gtk_widget_show (dlg);
		g_free (s);
	} else
		really_export_figure_postscript_cb (0, dialog);
}

static void
save_multiple_cb (GtkWidget * widget, gpointer data)
{
	GList *childList;
	gint childNumber, i = 0;
	DrGeniusMDIChild *child;
	GtkWidget *list;
	gchar *text[2], *dataName[] =
	{
		N_ ("Interactive 2D Figure"),
		N_ ("Console"),
		N_ ("Text Buffer"),
	};
	childList = mdi->children;
	childNumber = g_list_length (childList);
	if (childNumber == 0)
		return;
	if (xmlSessionWidget == NULL) {
		xmlSessionWidget = glade_xml_new (DRGENIUS_GLADEDIR"/drgenius.glade",
						  "dialogSaveSession");
		glade_xml_signal_autoconnect (xmlSessionWidget);
	} else
		gtk_widget_show (glade_xml_get_widget (xmlSessionWidget,
						       "dialogSaveSession"));
	list = glade_xml_get_widget (xmlSessionWidget, "sessionSelectList");
	gtk_clist_clear (GTK_CLIST (list));
	while (i < childNumber) {
		if ((child = (DrGeniusMDIChild *) g_list_nth_data (childList, i)) == NULL)
			break;
		text[1] = ((GnomeMDIChild *) child)->name;
		switch (child->child_type) {
		case FIGURE_CHILD:
			text[0] = _ (dataName[0]);
			break;
		case EDITOR_CHILD:
			text[0] = _ (dataName[2]);
			break;
		case GENIUSCON_CHILD:
			text[0] = _ (dataName[1]);
			break;
		}
		gtk_clist_append (GTK_CLIST (list), text);
		gtk_clist_set_row_data (GTK_CLIST (list), i, child);
		i++;
	}

	/* Next we add the list of avalaible macro-construction */
	text[0] = N_ ("Macro-construction");
	text[1] = firstMacroName ();
	while (text[1] != NULL) {
		gtk_clist_append (GTK_CLIST (list), text);
		text[1] = nextMacroName ();
	}
}

static void 
export_figure_to_latex_cb (GtkWidget * widget, gpointer data)
{
	DrGeniusMDIChild *child;
	gchar *fileName;
	gchar **split;
	GtkWidget *dlg;

	if (!gnome_mdi_get_active_child (mdi))
		return;
	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));
	/* only export figure right now */
	if (child->child_type != FIGURE_CHILD)
	{
		dlg = gnome_warning_dialog (_("Only geometric figure\ncan be exported to LaTex!"));
		gtk_window_position (GTK_WINDOW (dlg), GTK_WIN_POS_MOUSE);
		gtk_widget_show (dlg);
		return;
	}
	/* if we have a filename */
	if (child->filename)
	{
		split = g_strsplit( g_basename(child->filename),".", 2);
		fileName = g_strconcat (split[0], ".tex", NULL);
		g_strfreev (split);
	}
	else
		fileName = g_strdup (drgeoLatexFileName);

	dlg = gtk_file_selection_new (_ ("Export As..."));
	gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
	gtk_file_selection_set_filename (GTK_FILE_SELECTION (dlg),fileName);
	gtk_window_position (GTK_WINDOW (dlg), GTK_WIN_POS_MOUSE);
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->cancel_button),
				   "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (dlg));
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->ok_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (export_figure_latex_ok_cb),
			    (gpointer) dlg);
	gtk_widget_show (dlg);
	g_free(fileName);
}

static void
export_figure_to_postscript_cb (GtkWidget * widget, gpointer data)
{
	DrGeniusMDIChild *child;
	gchar *fileName;
	gchar **split;
	GtkWidget *dlg;

	if (!gnome_mdi_get_active_child (mdi))
		return;
	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));
	/* only export figure right now */
	if (child->child_type != FIGURE_CHILD)
	{
		dlg = gnome_warning_dialog (_("Only geometric figure\ncan be exported to PostScript!"));
		gtk_window_position (GTK_WINDOW (dlg), GTK_WIN_POS_MOUSE);
		gtk_widget_show (dlg);
		return;
	}
	/* if we have a filename */
	if (child->filename)
	{
		split = g_strsplit( g_basename(child->filename),".", 2);
		fileName = g_strconcat (split[0], ".eps", NULL);
		g_strfreev (split);
	}
	else
		fileName = g_strdup (drgeoPostscriptFileName);

	dlg = gtk_file_selection_new (_ ("Export As..."));
	gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
	gtk_file_selection_set_filename (GTK_FILE_SELECTION (dlg),fileName);
	gtk_window_position (GTK_WINDOW (dlg), GTK_WIN_POS_MOUSE);
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->cancel_button),
				   "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (dlg));
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->ok_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (export_figure_postscript_ok_cb),
			    (gpointer) dlg);
	gtk_widget_show (dlg);
	g_free(fileName);
}

static void
rename_view_cb (GtkWidget * widget, gpointer data)
{
	GnomeMDIChild *child;
	GnomeApp *app;

	if (!gnome_mdi_get_active_child (mdi))
		return;
	child = gnome_mdi_get_active_child (mdi);	
	app = gnome_mdi_get_active_window (mdi);
	gnome_app_request_string (app, _("Enter the name of this view:"),
				  rename_view_string_cb, (gpointer) child);
	
}

static void 
rename_view_string_cb (gchar *reply, gpointer child)
{
	if (!GNOME_IS_MDI_CHILD(child) || (reply == NULL))
		return;
	gnome_mdi_child_set_name(GNOME_MDI_CHILD(child), reply); 
}

static void
customize_interface_cb (GtkWidget * widget, gpointer data)
{
	DrGeniusMDIChild *child;

	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));

	switch (child->child_type) {
	case FIGURE_CHILD:
		adaptDrgeoDialog (child);
		break; 
	case EDITOR_CHILD:
		/* No toolbar for now */
		break;
	}
}

static void
new_figure_cb (GtkWidget * widget, gpointer data)
{
	drgenius_mdi_new_child (mdi, FIGURE_CHILD, NULL);	
	reconcile_grayout_widget ();
}

static void
new_editor_cb (GtkWidget * widget, gpointer data)
{
	drgenius_mdi_new_child (mdi, EDITOR_CHILD, NULL);
	reconcile_grayout_widget ();
}

static void
close_cb (GtkWidget * widget, gpointer data)
{
	GtkWidget *app;

	app = (GtkWidget *) data;

	drgenius_mdi_close_view (mdi);
	
	reconcile_grayout_widget ();
}

static void quitDrgenius (int reply, gpointer data)
{
	if (reply == 0) gtk_main_quit ();
}

void
exit_cb (GtkWidget * widget, gpointer data)
{
	GtkWidget *quit;	
       
	if (g_list_length (mdi->children) == 0)
		gtk_main_quit ();
	else	
		quit = gnome_question_dialog_modal 
			(_("Are you sure you want to quit?\nSome data may be unsaved"),
			 (GnomeReplyCallback) quitDrgenius,
			 NULL);
}


static void
about_cb (GtkWidget * widget, gpointer data)
{
	static GtkWidget *dialog = NULL;
	GtkWidget *app, *l, *hbox;

	app = (GtkWidget *) data;

	if (dialog != NULL) {
		g_assert (GTK_WIDGET_REALIZED (dialog));
		gdk_window_show (dialog->window);
		gdk_window_raise (dialog->window);
	} else {
		const gchar *authors[] =
		{
			N_ ("Hilaire Fernandes: main programmer, Dr Geo"),
			N_ ("The Gnome Translation Teams. Check the po/*.po files to know them"),
			N_ ("George Lebl: MDI interface design with GOB"),
			N_ ("Laurent Gauthier: MDI interface, many contributions in Dr Geo code"),
			/* Translators: Translate Fr&eacute;d&eacute;ric */
			N_ ("Frederic Toussaint: graphic art"),
			NULL
		};

		dialog = gnome_about_new ("Dr Genius", VERSION,
		   "(C) Copyright 1997-2002 Free Software Foundation",
					  authors,
					  "Dr Genius Refers to Geometry Exploration and Numeric Intuitive User System.",
					  "drgenius.png");

		hbox = gtk_hbox_new (TRUE, 0);
		l = gnome_href_new ("http://www.ofset.org/drgenius",
				    _ ("Dr Genius Home Page"));
		gtk_box_pack_start (GTK_BOX (hbox), l, FALSE, FALSE, 0);
		l = gnome_href_new ("http://www.ofset.org",
				    _ ("An OFSET development"));
		gtk_box_pack_start (GTK_BOX (hbox), l, FALSE, FALSE, 0);
		gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
				    hbox, TRUE, FALSE, 0);
		gtk_widget_show_all (hbox);



		gtk_signal_connect (GTK_OBJECT (dialog),
				    "destroy",
				    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
				    &dialog);

		gnome_dialog_set_parent (GNOME_DIALOG (dialog), GTK_WINDOW (app));

		gtk_widget_show (dialog);
	}
}

void 
on_sessionSaveSelection_clicked (GtkWidget * widget, gpointer data)
{
	GtkWidget *list;
	gchar *txt, *item;
	gint i = -1;
	DrGeniusMDIChild *child;
	GtkWidget *dialog;
	GtkWidget *app;

	app = glade_xml_get_widget (xmlSessionWidget, "dialogSaveSession");
	drgeniusXml = xmlNewDoc (BAD_CAST "1.0");
	drgeniusXml->root = xmlNewDocNode (drgeniusXml, NULL,
					   BAD_CAST "drgenius", NULL);

	list = glade_xml_get_widget (xmlSessionWidget, "sessionSelectList");
	while (gtk_clist_get_text (GTK_CLIST (list), ++i, 0, &txt)) {
		if (is_clist_row_selected (GTK_CLIST (list), i)) {
			// get the name of the mdi child
			gtk_clist_get_text (GTK_CLIST (list), i, 1, &item);			
			// we got one selected row
			if (!strcmp (txt, _ ("Interactive 2D Figure")) ||
			    !strcmp (txt, _ ("Text Buffer")) ) {
				// Check if we can find him in the MDI				
				if ((child = (DrGeniusMDIChild *) gnome_mdi_find_child (mdi, item)) != NULL) {
					// MDI child still alive, we can save its data
					drgenius_mdi_child_saveNode (child, drgeniusXml->root);
				}
			}
			else if (!strcmp (txt, _ ("Macro-construction"))) {
				printf ("%s\n", item);
				saveMacro (item, drgeniusXml->root);
			} else {
				char *s = g_strdup_printf ("on_sessionSaveSelection_clicked: Unknown data type '%s' to save\n", item);
				gnome_error_dialog (s);
				free (s);
			}
		}
	}

	dialog = gtk_file_selection_new (_ ("Save Session As..."));
	if (!sessionPathName)
	  gtk_file_selection_set_filename( GTK_FILE_SELECTION(dialog),globalSessionFileName);
	else
	  gtk_file_selection_set_filename( GTK_FILE_SELECTION(dialog),sessionPathName);

	gtk_window_position (GTK_WINDOW (dialog), GTK_WIN_POS_MOUSE);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (app));
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (dialog)->cancel_button),
				   "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (dialog));
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dialog)->ok_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (save_session_ok_cb),
			    (gpointer) dialog);
	gtk_widget_show (dialog);

	gtk_widget_hide (glade_xml_get_widget (xmlSessionWidget, "dialogSaveSession"));
}
void 
on_sessionSaveAll_clicked (GtkWidget * widget, gpointer data)
{
	GtkWidget *list;
	list = glade_xml_get_widget (xmlSessionWidget, "sessionSelectList");
	gtk_clist_select_all (GTK_CLIST (list));
	return on_sessionSaveSelection_clicked (widget,data);
}

void 
on_sessionCancel_clicked (GtkWidget * widget, gpointer data)
{
	gtk_widget_hide (glade_xml_get_widget (xmlSessionWidget, "dialogSaveSession"));
}

static gboolean
is_clist_row_selected (GtkCList * clist, gint row)
{
	GList *node = g_list_first (clist->selection);
	while (node) {
		if (row == GPOINTER_TO_INT (node->data))
			return (TRUE);
		node = g_list_next (node);
	}
	return (FALSE);
}

void reconcile_grayout_undo (void)
{
	DrGeniusMDIChild *child;
	child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));
	if (drgenius_mdi_child_undoActive (child))
	{
		gtk_widget_set_sensitive (toolbar[3].widget, TRUE);
		gtk_widget_set_sensitive (edit_menu[0].widget, TRUE);
	}
	else
	{
		gtk_widget_set_sensitive (toolbar[3].widget, FALSE);
		gtk_widget_set_sensitive (edit_menu[0].widget, FALSE);
	}
	if (drgenius_mdi_child_redoActive (child))
	{
		gtk_widget_set_sensitive (edit_menu[1].widget, TRUE);
		gtk_widget_set_sensitive (toolbar[4].widget, TRUE);
	}
	else
	{
		gtk_widget_set_sensitive (edit_menu[1].widget, FALSE);
		gtk_widget_set_sensitive (toolbar[4].widget, FALSE);
	}
}
void reconcile_grayout_widget (void)
{
	DrGeniusMDIChild *child;
	GtkWidget *active_view;

	active_view = gnome_mdi_get_active_view (mdi);
	/* Gray out or enable buttons/menus */
	if (active_view)
	{
		child = DRGENIUS_MDI_CHILD (gnome_mdi_get_active_child (mdi));
		/* undo-redo menus/buttons */
		reconcile_grayout_undo ();
		gtk_widget_set_sensitive (edit_menu[3].widget, TRUE);
		gtk_widget_set_sensitive (file_menu[4].widget, TRUE);

		/* save menu */
		if (child->child_type == FIGURE_CHILD) {
			gtk_widget_set_sensitive (file_menu[2].widget, TRUE); 
			gtk_widget_set_sensitive (file_menu[3].widget, TRUE);
			gtk_widget_set_sensitive (file_menu[5].widget, TRUE);
			gtk_widget_set_sensitive (edit_menu[4].widget, TRUE);
		}
		else if (child->child_type == EDITOR_CHILD){
			gtk_widget_set_sensitive (file_menu[2].widget, FALSE); 
			gtk_widget_set_sensitive (file_menu[3].widget, FALSE);
			gtk_widget_set_sensitive (file_menu[5].widget, FALSE);		
			gtk_widget_set_sensitive (edit_menu[4].widget, FALSE);
		}
	}
	else
	{
		/* not active view,
		   grey out save, filter and undo/redo menus/buttons */
		gtk_widget_set_sensitive (file_menu[2].widget, FALSE);
		gtk_widget_set_sensitive (file_menu[3].widget, FALSE);
		gtk_widget_set_sensitive (file_menu[4].widget, FALSE);
		gtk_widget_set_sensitive (file_menu[5].widget, FALSE);		
		gtk_widget_set_sensitive (edit_menu[0].widget, FALSE);
		gtk_widget_set_sensitive (edit_menu[1].widget, FALSE);
		gtk_widget_set_sensitive (edit_menu[3].widget, FALSE);
		gtk_widget_set_sensitive (edit_menu[4].widget, FALSE);
		gtk_widget_set_sensitive (toolbar[3].widget, FALSE);
		gtk_widget_set_sensitive (toolbar[4].widget, FALSE);
	}
}


/* ----------------- Preferences box CODE  -------------------- */
static void
preferences_box_cb(GtkWidget * widget, gpointer data)
{
  initPreferencesBox ();
}










