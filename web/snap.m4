m4_include(mylib.m4)
<HTML>
_HEAD(`Dr.&nbsp;Genius snapshot')
_BODY(ac536.png)

_MAIN
_SUB_BODY_TOP

<CENTER><H1><U>Snapshot with legend</U></H1></CENTER><HR><BR>

<p>
Here is some snapshots of Dr.&nbsp;Genius in action:<br>
<center>
<p>
_LINK(sshot1.png, _INCLUDE_IMG(sshot1-thumb.png,Thumbnail))&nbsp;
<p>
_LINK(sshot2.png, _INCLUDE_IMG(sshot2-thumb.png,Thumbnail))&nbsp;
<p>
_LINK(sshot3.png, _INCLUDE_IMG(sshot3-thumb.png,Thumbnail))&nbsp;
</center>
<p>
You can also look at some older _LINK(http://drgeo.seul.org,Dr Geo)
or _LINK(http://www.5z.com/jirka/genius.html,Genius) snapshots

_END_MAIN

_FOOT()
</BODY>
</HTML>
