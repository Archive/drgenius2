m4_include(mylib.m4)
<html>
_HEAD(`Dr.&nbsp;Genius _VERSION Bug Report ')
_BODY(ac536.png)

_MAIN
_SUB_BODY_TOP

<p>
We're doing bug-tracking via the
_LINK(http://bugs.gnome.org/,GNOME bug tracking system).
This page includes
_LINK(http://bugs.gnome.org/Reporting.html,instructions) on how to format
your submissions.
Basically, at the top of your email to submit@bugs.gnome.org include the
two lines:</p>
<pre>
Package: drgenius
Version: _VERSION
</pre>
<p>
and <em>report separate bugs in separate bug reports</em>.
If you are not using the latest version of Dr.&nbsp;Genius, please upgrade; the bug
may have gone away.
</p>
<p>
_LINK(http://bugs.gnome.org/db/pa/ldrgenius.html,Outstanding Bugs).
</p>

_END_MAIN

_FOOT()
</body>
</html>
