 m4_include(mylib.m4)
<HTML>
_HEAD(`Dr.&nbsp;Genius FAQ')
_BODY(ac536.png)

_MAIN
_SUB_BODY_TOP

<CENTER><H1><U>FAQ</U></H1></CENTER><HR><BR><BR>

m4_changequote(<<,>>)

m4_define(<<_INDEX>>,<<Index>>)m4_dnl
m4_define(<<_Q1>>,<<What does Dr.&nbsp;Genius stand for?>>)m4_dnl
m4_define(<<_Q2>>,<<What is Dr.&nbsp;Genius?>>)m4_dnl
m4_define(<<_QroadMap>>,<<What is the road map for Dr.&nbsp;Genius?>>)m4_dnl
m4_define(<<_Q3>>,<<What are the system requirements for Dr.&nbsp;Genius?>>)m4_dnl
m4_define(<<_Q4>>,<<Where can I get Dr.&nbsp;Genius?>>)m4_dnl
m4_define(<<_Q5>>,<<How do I compile and install the packages?>>)m4_dnl
m4_define(<<_Q6>>,<<Can I get RPM ( Redhat or SuSE ) or .deb ( Debian ) binaries?>>)m4_dnl
m4_define(<<_Q7>>,<<So now I've got Dr.&nbsp;Genius. What do I do with it?>>)m4_dnl
m4_define(<<_Q8>>,<<Help !! Where can I get some help?>>)m4_dnl
m4_define(<<_Q9>>,<<Are there any manuals/documentation?>>)m4_dnl
m4_define(<<_Q10>>,<<How do I report a bug? or a wishlist item?>>)m4_dnl
m4_define(<<_Q11>>,<<I want to get involved. How can I help?>>)m4_dnl
m4_define(<<_Q12>>,<<How do I add a question and/or answer to this faq?>>)m4_dnl
m4_define(<<_Q13>>,<<Who are the authors of Dr.&nbsp;Genius? How did all of this started?>>)m4_dnl

m4_changequote(`,')

<A NAME=_INDEX><H2>FAQ index</H2></A>
<UL>
 <LI>_LINK_TO_LABEL(_Q1,_LQ1)
 <LI>_LINK_TO_LABEL(_Q2,_LQ2)
 <LI>_LINK_TO_LABEL(_QroadMap,_LQroadMap)
 <LI>_LINK_TO_LABEL(_Q3,_LQ3)
 <LI>_LINK_TO_LABEL(_Q4,_LQ4)
 <LI>_LINK_TO_LABEL(_Q5,_LQ5)
 <LI>_LINK_TO_LABEL(_Q6,_LQ6)
 <LI>_LINK_TO_LABEL(_Q7,_LQ7)
 <LI>_LINK_TO_LABEL(_Q8,_LQ8)
 <LI>_LINK_TO_LABEL(_Q9,_LQ9)
 <LI>_LINK_TO_LABEL(_Q10,_LQ10)
 <LI>_LINK_TO_LABEL(_Q11,_LQ11)
 <LI>_LINK_TO_LABEL(_Q12,_LQ12)
 <LI>_LINK_TO_LABEL(_Q13,_LQ13)
</UL>
<BR><HR><BR>

_SECTION_HEADER(_LQ1,_Q1,<P><B>D</B>r <B>G</B>enius <B>R</B>efers to
<B>G</B>eometric <B>E</B>xploration and <B>N</B>umeric
<B>I</B>ntuitive <B>U</B>ser <B>S</B>ystem</P>,_INDEX)
		
_SECTION_HEADER(_LQ2,_Q2, 
<P>Dr.&nbsp;Genius is an interactive geometry program and was also: [ plus a calculator
similiar in some aspects to bc and Matlab. Dr.&nbsp;Genius is powered by
GEL`,' its extention language. In fact`,' a large part of the standard
calculator functions are written in GEL itself.]
<P>Dr.&nbsp;Genius is a piece of GNU software. This means you don't have to pay
for it but even more importantly you have access to the source
code. You can modify and distribute it as long as the same
distribution license (GPL) is used.
<P>Dr.&nbsp;Genius is released under the GPL license. To learn more about
this license and the Free Software Foundation`,' visit the
GNU _LINK(http://www.gnu.org,web site) or read the file COPYING in
thedistribution.
<P>Dr. Genius is part of _LINK(http://www.gnome.org,the Gnome project).
</P>,_INDEX)

_SECTION_HEADER(_LQroadMap,_QroadMap,<P>
Master plan for Dr.&nbsp;Genius is as following:

<ol>
 <li>Implemeting Guile scriptability.
 <li>Defining a DrGeo API to be used for guile scriptability.
 <li>Implement a geometry property checker.
 <li>CORBA use for distance control over a figure.
 <li>And of course polishing the software is a permanent objective.

</ol>

More info can be found on the TODO file or in the
_LINK(todo.html,TODO) secton of this web site.

</p>,_INDEX)


_SECTION_HEADER(_LQ3,_Q3,<P>
<UL>
 <LI>A Unix compatible operating system
 <LI>October (or better) version of _LINK(http://www.gnome.org,GNOME)
 <LI>GPM Library. The _LINK(http://www.swox.com/gmp/, GNU Multiple Precision library).
</UL>
</P>,_INDEX)

_SECTION_HEADER(_LQ4,_Q4,<P>
You can get the latest version of Dr.&nbsp;Genius _LINK(download.html,there).
</P>,_INDEX)

_SECTION_HEADER(_LQ5,_Q5,<P>
Do the following command:
<P><CODE>tar xfz drgenius-_VERSION.tar.gz<BR>
cd drgenius2<BR>
./autogen;make<BR>
su (enter the root password there)<BR>
make install<BR>
exit<BR>
drgenius</CODE>
</P>,_INDEX)

_SECTION_HEADER(_LQ6,_Q6,<P>
RPM are available from here at the download sections.
<p>Debian package are available as well for Debian 2.2 in the unstable branch actually.
</P>,_INDEX)

_SECTION_HEADER(_LQ7,_Q7,<P>
Try it (FIXME there)
</P>,_INDEX)

_SECTION_HEADER(_LQ8,_Q8,<P>
You can get help at the Dr.&nbsp;Genius mailing list which you
can join following the instruction at _LINK(ml.html,there).
</P>,_INDEX)

_SECTION_HEADER(_LQ9,_Q9,<P>
The documentation is in progress and only
avalaibe from the _LINK(cvs.html,cvs).
Your help is welcome.
</P>,_INDEX)

_SECTION_HEADER(_LQ10,_Q10,<P>
We're doing bug-tracking via the 
_LINK(http://bugs.gnome.org,GNOME bug tracking system).
In this page you will find instructions on  how to
format your submissions.
Basically`,' at the top of your email to "submit@bugs.gnome.org"
include the two lines:
<CODE><P>
Package: drgenius<BR>
Version: [ whatever version you are using ie: _VERSION ]</CODE>
<P>Please make sure that you report separate bugs in separate reports.
</P>,_INDEX)

_SECTION_HEADER(_LQ11,_Q11,<P>
There's lots to do. even if you are not a programmer. You can start by visiting the _LINK(todo.html,todo) page.
</P>,_INDEX)

_SECTION_HEADER(_LQ12,_Q12,<P>
Just send an email to the Dr.&nbsp;Genius _LINK(ml.html,developer mailing list).
</P>,_INDEX)

_SECTION_HEADER(_LQ13,_Q13,<P>
     Authors :<BR>
     Hilaire Fernandes<BR>
     George Lebl<BR>

     <P><B>Other people</B> are also involved in Dr.&nbsp;Genius:<BR>

     Laurent Gauthier (MDI interface, redesign in the GUI interface)<BR>
     Nils Barth (Math. library written in GEL, documentation)<BR>
     Fr�d�ric Toussaint (Graphic art)<BR>

     <P>Dr.&nbsp;Genius is the merge of two projects know as Gnome Genius
     Caculator from George Lebl and  GTK Dr.&nbsp;Geo from Hilaire Fernandes.
     <P>Gnome Genius Calculator is a multiple precision calculator
     with an interpreted language`,' GEL`,' plus a large (and growing!)
     mathematical library written in GEL.

     <P>Dr.&nbsp;Geo is a sort of vector drawing software but with
     mathematical constraints - we also call this interactive geometry.
</P>,_INDEX)

_END_MAIN

_FOOT()
</BODY>
</HTML>





