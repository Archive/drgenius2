m4_include(mylib.m4)
<HTML>
_HEAD(`Dr.&nbsp;Genius Log')
_BODY(ac536.png)

_MAIN
_SUB_BODY_TOP

<P>
<H2>New in Dr.&nbsp;Genius:</H2>

<ul> New in v 0.5.15

 <li>Implement a description feature for each open figure in
 a tree. A description tree is attached in a vertical panel at the
right of a figure.

 <li>Various bugs fix.

 <li> Update the screenshot area with representative views.

</ul>

<ul> New in v 0.5.14

 <li>New polygon object. This include defining default color setup,
style setup for this object. Also implemented for the Postscript &
LaTeX drawable. The polygon object is not really a geometric object as
it cannot interact with other geometric object (ie no intersection for
example).

</ul>

<ul> New in v 0.5.13

 <li>Bug fixes with the locale. Now tips message over geometric object are
correctly handled. NUMERIC locale is forced to C.

 <li>Bug fixes in the tips behavior.

 <li>Fix problem with GOB >= 1.0.10.


</ul>


<ul> New in v 0.5.12

 <li>Various bugs fix: black screen bug, flickering mouse/tops bugs,
parallel line bug.

 <li>New user preference dialog: the user can adjust the default file
names, undo/redo level, geometric object style. All the preferences
are saved.

 <li>Redesigned style dialog. The new dialogs are more
intuitive. Different dialogs are used according to the geometric
object.

 <li>New preference dialogs. These dialogs are used to adjust specific
data of geometric object. (ie the coordinate of a free point and the
value of a free value object).

 <li>Due to change in GNU Gettext, the message system in DrGenius is
partially broken, I hope to fix it soon.

</ul>

<ul>New in v 0.5.11
 <li>The unmaintained genius calculator is removed from the
distribution. This produce a smaller application.
 <li> Figure can be renamed, the function is in the Edit menu.
</ul>

<ul>New in v 0.5.10
 <li>A super tetra cool buggy multi-level undo/redo system for the
geometric engine.

</ul>

<ul>New in v 0.5.9
 <li>Moslty a fix over version 0.5.8 that didn't compile.
 <li>Put the export menu item under a sub menu. Thanks to Fr�d�ric
Bonnaud.
</ul>

<ul>New in v 0.5.8
 <li>There is an eps exporter for geometric figure, this is a patch
from Fr�d�ric Bonnaud based on the latex exporter.

 <li>The geometry engine uses double buffering when the user moves the
figure. This is a X server side process so should be usable with
X-terminals.

 <li>There is now a specific tool-bar for geometric child. The
tool-bar comes with sub-tool-bars. These make the application more
easy for kids while looking for the different functions.

 <li>The canvas - the stuff to display function curve - is no more
part of Dr.&nbsp;Genius, it was unsupported and need more work. External
solution should be use instead.
</ul>

<ul>New in v 0.5.7
 <li>Laurent and Fabrice Bellet (Fabrice.Bellet@creatis.insa-lyon.fr)
fix a detection problem of GMP 3.x in configure.in
 <li>Exporting to LaTex now covers all kind of object. It's now
possible to choose the file name to export.
 <li> In the geometric engine, the style of object: clarify the situation
between invisible (in situation when it needs to be displayed) and
visible dashed line. Invisible line are displayed dashed with two
colors: yellow and the original. Visible dashed line are displayed as
expected.
</ul>

<ul>New in v 0.5.6
 <li>George remove the GOB tree from Dr.&nbsp;Genius. This means GOB
needs to be installed separatly. You can get it
_LINK(http://www.5z.com/jirka/gob.html, there).
 <li>Hilaire has implemented a new geometric engine. Most of the
functionnality are the same right now but the code is cleaner and
smaller. By incidence the xml format of the geometric figure and
macro-construction have changed. It's simpler. Look at
dr-genius/drgeo/DeveloperFAQ for more.
 <li>Laurent has submitted a couple of geometric figures with
locus. With that Hilaire has improved a bit the locus engine but it's a bit
slow.
 <li>Hilaire is working on a Latex export facility using PSTricks.
</ul>

<ul>New in v 0.5.5
 <li>George is working hard on the new genius interface. So we (canvas
& figure) will be able to interact with a console.
 <li>The XML stuff is finished, so it's now possible to save/load
multiple data at once. The data are  now figure & macro but the
interface is there to save other kind of data when ready.
 <li>Dr Geo view now use the George's GOB GTK builder to create new
MDI child.
</ul>

<ul>New in v 0.5.0
 <li>Integration of the Gnome Genius Calculator and GTK Dr Geo.
 <li>New GUI interace, including MDI with Genius, Dr Geo, GEL buffer view.
 <li>XML file format, still in progress, to save all kind of mixed data from the MDI view.
</ul>

<P>
<H2>Recent chage in the CVS:</H2>
_LINK(http://cvs.gnome.org/lxr/source/drgenius2/ChangeLog,Dr.&nbsp;Genius - Main ChangeLog)<BR>
_LINK(http://cvs.gnome.org/lxr/source/drgenius2/drgeo/ChangeLog, Dr.&nbsp;Genius - Geometric engine ChangeLog)
<p>Change Log in the oldest CVS module (dr-genius):<br>
_LINK(http://cvs.gnome.org/lxr/source/dr-genius/ChangeLog, Dr.&nbsp;Genius - Console Genius calculator ChangeLog)<BR>

<p>Here are the code commits during the last
_LINK(http://cvs.gnome.org/bonsai/cvsquery.cgi?module=all&branch=&branchtype=match&dir=drgenius2&file=&filetype=match&who=&whotype=match&sortby=Date&hours=2&date=week&mindate=&maxdate=&cvsroot=%2Fcvs%2Fgnome,week).

<BR>

_END_MAIN

_FOOT()
</BODY>
</HTML>


