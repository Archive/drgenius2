/*
 *  Dr Genius interactive geometry software
 * (C) Copyright Hilaire Fernandes  2002
 * 
 * 
 * 
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <crypt.h>
#include <string.h>

#include <glade/glade.h>
#include "drgeo_adaptDialog.h"
#include "drgeo/drgeo_figure.h"
#include "drgeo_menu.h"

static GladeXML *xml = NULL;
static drgeoFigure *myFigure;
static DrGeniusMDIChild *myChild;
static GtkWindow *dialog;
gchar *toolName[]=
{"pointMenu","curveMenu","transformationMenu","numericMenu",
 "macroMenu","otherMenu","moveItem","freePoint","middle",
 "intersection","coordinatesPoint","line","halfLine","segment",
 "vector","circle","arcCircle","locus","polygon",
 "parallel","perpendicular","reflexion","symmetry","translation",
 "rotation","scale","distance","angle","equation","script",
 "buildMacro","runMacro","widgetScript","deleteItem",
 "styleItem","propertyItem"};


static void updateDrgeoDialog ()
{
	/* update the dialog according to the unselected tools in this
	 * figure */
	gint i;

	for (i = 0; i <  DRGEO_TOOLS_NUMBER; i++)
		setSensitiveState (toolName[i],
				   myFigure->getToolState ((drgeoToolId) i));
}

void adaptDrgeoDialog (DrGeniusMDIChild *child)
/* Adapt the Drgeo dialog */
{
	GtkWidget *w;

	if (xml != NULL)
		/* only one adaptDrgeoDialog at a time */
		return;
	myFigure = (drgeoFigure *) drgeo_mdi_child_get_figure (child);
	myChild = child;

	xml = glade_xml_new (DRGENIUS_GLADEDIR"/drgeo.glade",
			     "uiAdaptDialog");
	dialog = GTK_WINDOW (glade_xml_get_widget (xml, "uiAdaptDialog"));
	glade_xml_signal_autoconnect (xml);

	/* Force the toolbar style, we do not want the default one from the Gnome Desktop */
	w = glade_xml_get_widget (xml, "tb0");
	gtk_toolbar_set_style (GTK_TOOLBAR (w), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (w), GTK_RELIEF_HALF);
	w = glade_xml_get_widget (xml, "tb1");
	gtk_toolbar_set_style (GTK_TOOLBAR (w), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (w), GTK_RELIEF_NONE);
	w = glade_xml_get_widget (xml, "tb2");
	gtk_toolbar_set_style (GTK_TOOLBAR (w), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (w), GTK_RELIEF_NONE);
	w = glade_xml_get_widget (xml, "tb2");
	gtk_toolbar_set_style (GTK_TOOLBAR (w), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (w), GTK_RELIEF_NONE);
	w = glade_xml_get_widget (xml, "tb3");
	gtk_toolbar_set_style (GTK_TOOLBAR (w), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (w), GTK_RELIEF_NONE);
	w = glade_xml_get_widget (xml, "tb4");
	gtk_toolbar_set_style (GTK_TOOLBAR (w), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (w), GTK_RELIEF_NONE);
	w = glade_xml_get_widget (xml, "tb5");
	gtk_toolbar_set_style (GTK_TOOLBAR (w), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (w), GTK_RELIEF_NONE);
	w = glade_xml_get_widget (xml, "tb6");
	gtk_toolbar_set_style (GTK_TOOLBAR (w), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_button_relief (GTK_TOOLBAR (w), GTK_RELIEF_NONE);
	
	updateDrgeoDialog ();
}

void on_pointMenuHide_clicked (GtkWidget *w, gpointer data)
{
	gboolean activated;
	
	activated = !getSensitiveState (w);
	gtk_widget_set_sensitive (GTK_BIN(w)->child, activated);
	/* set the state of the tools packed in this barre */
	setSensitiveState ("freePoint", activated);
	setSensitiveState ("middle", activated);
	setSensitiveState ("intersection", activated);
	setSensitiveState ("coordinatesPoint", activated);
	
}
void on_curveMenuHide_clicked (GtkWidget *w, gpointer data)
{
	gboolean activated;
	
	activated = !getSensitiveState (w);
	gtk_widget_set_sensitive (GTK_BIN(w)->child, activated);
	/* set the state of the tools packed in this barre */
	setSensitiveState ("line", activated);
	setSensitiveState ("halfLine", activated);
	setSensitiveState ("segment", activated);
	setSensitiveState ("vector", activated);
	setSensitiveState ("circle", activated);
	setSensitiveState ("arcCircle", activated);
	setSensitiveState ("locus", activated);
	setSensitiveState ("polygon", activated);
}
void on_transformationMenuHide_clicked (GtkWidget *w, gpointer data)
{
	gboolean activated;
	
	activated = !getSensitiveState (w);
	gtk_widget_set_sensitive (GTK_BIN(w)->child, activated);
	/* set the state of the tools packed in this barre */
	setSensitiveState ("parallel", activated);
	setSensitiveState ("perpendicular", activated);
	setSensitiveState ("reflexion", activated);
	setSensitiveState ("symmetry", activated);
	setSensitiveState ("translation", activated);
	setSensitiveState ("rotation", activated);
	setSensitiveState ("scale", activated);
}
void on_numericMenuHide_clicked (GtkWidget *w, gpointer data)
{
	gboolean activated;
	
	activated = !getSensitiveState (w);
	gtk_widget_set_sensitive (GTK_BIN(w)->child, activated);
	/* set the state of the tools packed in this barre */
	setSensitiveState ("distance", activated);
	setSensitiveState ("angle", activated);
	setSensitiveState ("equation", activated);
	setSensitiveState ("script", activated);
}
void on_macroMenuHide_clicked (GtkWidget *w, gpointer data)
{
	gboolean activated;
	
	activated = !getSensitiveState (w);
	gtk_widget_set_sensitive (GTK_BIN(w)->child, activated);
	/* set the state of the tools packed in this barre */
	setSensitiveState ("buildMacro", activated);
	setSensitiveState ("runMacro", activated);
	setSensitiveState ("widgetScript", activated);
}
void on_otherMenuHide_clicked (GtkWidget *w, gpointer data)
{
	gboolean activated;
	
	activated = !getSensitiveState (w);
	gtk_widget_set_sensitive (GTK_BIN(w)->child, activated);
	/* set the state of the tools packed in this barre */
	setSensitiveState ("moveItem", activated);
	setSensitiveState ("deleteItem", activated);
	setSensitiveState ("styleItem", activated);
	setSensitiveState ("propertyItem", activated);
}
void on_toolHide_clicked (GtkWidget *w, gpointer data)
{
	if (getSensitiveState (w))
		gtk_widget_set_sensitive (GTK_BIN(w)->child, FALSE);
	else
		gtk_widget_set_sensitive (GTK_BIN(w)->child, TRUE);
}

void on_uiAdaptDialogClose_clicked (GtkWidget *w, gpointer data)
{
	gtk_widget_destroy (GTK_WIDGET (dialog));
	gtk_object_destroy (GTK_OBJECT (xml));
	xml = NULL;
	/* Eventually needed */
	reconcile_popup_menu (myChild);
}

void on_uiAdaptDialogLock_clicked (GtkWidget *b, gpointer data)
{
	GtkWidget *w;
	/* Ask for a password if none was specified before*/
	if (myFigure->getPassword())
	{
		/* Ask the previous password when it is not null */
		w = gnome_request_dialog (TRUE, 
					  _("This interface has already been locked\nEnter the previously used password for this view: "),
					  NULL, (guint16) 20, (GnomeStringCallback) askPasswordToRelock,
					  NULL, dialog);
	}
	else
	{
		w = gnome_request_dialog (TRUE, 
					  _("Password to lock the changes to the interface: "),
					  NULL, (guint16) 20, (GnomeStringCallback) askPasswordToLock,
					  NULL, dialog);
	}
	gtk_window_set_modal (GTK_WINDOW (w), TRUE);
}

void askPasswordToLock (gchar *password, gpointer data)
{
	gint i;
	unsigned long seed[2];
	char salt[] = "$1$........";
	const char *const seedchars =
		"./0123456789ABCDEFGHIJKLMNOPQRST"
		"UVWXYZabcdefghijklmnopqrstuvwxyz";
	char *cryptPassword;

	if (password == NULL)
		return;
     
	seed[0] = time(NULL);
	seed[1] = getpid() ^ (seed[0] >> 14 & 0x30000);
	
	/* Turn it into printable characters from `seedchars'. */
	for (i = 0; i < 8; i++)
		salt[3+i] = seedchars[(seed[i/5] >> (i%5)*6) & 0x3f];

	cryptPassword = crypt (password, salt);

	/* Save the crypted password into the figure */
	myFigure->setPassword (cryptPassword);

	/* Record and Update the changes to the UI */
	for (i = 0; i <  DRGEO_TOOLS_NUMBER; i++)
		myFigure->setToolState ((drgeoToolId) i,
				      getSensitiveStateByName (toolName[i]));
	drgenius_mdi_child_update_user_interface (myChild);
	
}

void askPasswordToRelock (gchar *password, gpointer data)
{
	/* The interface is already locked, ask the password to relock
	   it with the new customization */
	char *result;
	gint i;

	if (password == NULL)
		return;
       
	result = crypt(password, 
		      myFigure->getPassword ());

	if (!strcmp (result, myFigure->getPassword ()))
	{
	       /* access granted */
	       /* Record and Update the changes to the UI */
		for (i = 0; i <  DRGEO_TOOLS_NUMBER; i++)
			myFigure->setToolState ((drgeoToolId) i,
						getSensitiveStateByName (toolName[i]));
		drgenius_mdi_child_update_user_interface (myChild);
	}
	else
	{
		/* access denied */
		gnome_error_dialog_parented (_("Password does not match\n Access denied!"),dialog);
	}
}

void askPasswordToUnlock (gchar *password, gpointer data)
{
       char *result;
       gint i;

       if (password == NULL)
	       return;
       

       result = crypt(password, 
		      myFigure->getPassword ());
       if (!strcmp (result, myFigure->getPassword()))
       {
	       /* access granted */
	       /* Set visible all the tool in the interface */
	       for (i = 0; i <  DRGEO_TOOLS_NUMBER; i++)
		       myFigure->setToolState ((drgeoToolId) i, TRUE);
	       drgenius_mdi_child_update_user_interface (myChild);
	       /* forget the password */
	       myFigure->setPassword (NULL);
	       updateDrgeoDialog ();
       }
       else
       {
	       /* access denied */
	       gnome_error_dialog_parented (_("Password does not match\n Access denied!"),dialog);
       }
}

void on_uiAdaptDialogUnlock_clicked (GtkWidget *w, gpointer data)
{
	if (myFigure->getPassword ())
	{
		w = gnome_request_dialog (TRUE, 
					  _("Password to unlock the changes to the interface:"),
					  NULL, (guint16) 20, (GnomeStringCallback) askPasswordToUnlock,
					  NULL, dialog);
	
		gtk_window_set_modal (GTK_WINDOW (w), TRUE);
	}
}

gint on_uiAdaptDialog_delete_event (GtkWidget *w, GdkEventAny *e, gpointer data)
{
	gtk_object_destroy (GTK_OBJECT (xml));
	gtk_widget_destroy (GTK_WIDGET (dialog));
	xml = NULL;
	/* Eventually needed */
	reconcile_popup_menu (myChild);
	return FALSE;
}


gboolean getSensitiveState (GtkWidget *button)
/* return the senstitive state of the button */
{
	GtkArg args[1];
	
	args[0].name = "sensitive";
	gtk_object_getv (GTK_OBJECT (GTK_BIN(button)->child),
			 1, args);
	return GTK_VALUE_BOOL (args[0]);
}

gboolean getSensitiveStateByName (gchar *buttonName)
{
	return (getSensitiveState 
		(glade_xml_get_widget (xml, buttonName)));
}

void setSensitiveState (gchar *button, gboolean state)
{
	GtkWidget *w;

	w = glade_xml_get_widget (xml, button);
	gtk_widget_set_sensitive (GTK_BIN(w)->child, state);
}
