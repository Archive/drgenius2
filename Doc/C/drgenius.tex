% This is the documentation of DrGenius
% Copyright 2002 Hilaire Fernandes
% Licensed under the terms of the FDL

\documentclass[a4paper,11pt]{book}
\usepackage{hyperlatex}
\T \usepackage{graphicx}

% Prepare macro
\newcommand{\drgeniusImagePath}{figures}
\newcommand{\drgeniusApiOutLocale}{Retourne}
\newcommand{\drgeniusApiExampleLocale}{Exemple}
% Include common definitions
\input{../drgeniusCommon.tex}


\htmltitle{Dr. Genius User's Manual} 



\htmladdress{\small Any comments/remarks? \\
  Or you are volunteer to write part of the manual.\\
  -> Contact Hilaire Fernandes at OFSET (hilaire@ofset.org) or join
  the \drgenius mailing list.}


\title{Dr. Genius User's  Manual}
\author{Hilaire Fernandes \\
With contributions from David Bucknell \\
\textit{OFSET}\\
\texttt{http://www.ofset.org}
}



\begin{document}

\maketitle

\xname{contents}
\tableofcontents

\xname{introduction}
\chapter{Introduction}

Dr. Genius is the interactive geometry package for the Gnome Desktop.
It allows users to create geometric figures that can be manipulated
with respect to geometric rules. The program is useable with students
from the primary through secondary levels.

\drgeniusFigureSize{Dr. Genius welcome screen}{fig1}{8}

To create a new empty geometric figure, the user clicks on the first
(on the left) button in the toolbar. Alternatively, a new figure can
be created using the menu item \drgeniusMenu{File->New->Figure} from
the menu bar. When a new figure is created, a new toolbar with a set
of six icons appears. The last item (on the right) in the toolbar is
an option menu; it is used to adjust the scale of the current
geometric figure.

\drgeniusFigureSize{An empty geometric figure}{fig2}{6}

The six icons correspond to different types of functions.  These
functions are described in the next chapter.

\drgeniusFigureSize{A Dr.~Genius figure and its description}{fig50}{8}

With each figure, a pane is available. By default the pane is pushed
to the extreme left so that only the representation of the figure is
visible. At any moment, the user can push the pane to the right to
make visible the figure description. A figure description is a tree
list. The tree is composed of all the elements of the figure. The
elements relative to other elements can be expanded by pressing the
'+' sign, that way the user can visualize the parents of this element.

\chapter{Basic Functions}

This chapter describes the tools used to construct geometric
figure. At the end of this chapter, customisation is presented.

\section{Construction Tools}

These tools are split into six groups available from the second
toolbar of \drgenius.

\drgeniusFigureSize{Dr.~Genius's tool categories and their descriptions}{fig3}{12}

\xname{point_tools}
\subsection{Point Tools}

\subsubsection{Free Point}
\drgeniusIcon{fig4}

Create a free point on an area or a free point on an uni-dimensional
object. In the first case, the created point can be moved anywhere in
the figure area. To build the point the user just clicks on the
background. In the latter case, it is constrained to the
uni-dimensional object (line); it is stuck to the object. To build
this kind of point the user clicks over a line (i.e. straight line,
ray, segment, circle, arc, etc.).

\subsubsection{Midpoint}
\drgeniusIcon{fig5}

Create the middle of two points or the middle of a segment. In the
first case, the user selects two points. In the latter case the user
just selects a segment.

\subsubsection{Intersection}
\drgeniusIcon{fig6}

Create the intersection point(s) of two line objects. The user needs
to select two line objects.

\subsubsection{Point by its Coordinates}
\drgeniusIcon{fig7}

Create a point defined by its coordinates. The user needs to select
two numbers, the first selected number is the abscissa; the second one
is the ordinate.

\xname{line_tools}
\subsection{Line Tools}

\subsubsection{Straight Line}
\drgeniusIcon{fig8}

Create a straight line defined by two points. The user selects two
points.

\subsubsection{Ray}
\drgeniusIcon{fig9}

Create a ray (half-line) defined by two points. The user selects two
points; the first point is the origin, the second one belongs to the
ray.

\subsubsection{Segment}
\drgeniusIcon{fig10}

Create a segment given by two points.

\subsubsection{Vector}
\drgeniusIcon{fig11}

Create a vector given by two points. The user selects two points. The
first point is the origin; the second one is the extremity.

\subsubsection{Circle}
\drgeniusIcon{fig12}

Create a circle. The user can create a circle from different
selections:
\begin{enumerate}
 \item the centre and a point on the circle;
 \item the centre and a number (the radius of the circle);
 \item the centre and a segment which length is the radius of the circle. 
\end{enumerate}

\subsubsection{Arc Circle}
\drgeniusIcon{fig13}

Create an arc defined by three points. The first selected point is the
origin of the arc; the third one is the extremity and the second one
is a point on the arc.

\subsubsection{Locus}
\drgeniusIcon{fig14}

Create a locus defined by two points. The user selects two points, one
is a free point on a line and the other one is a constrained point of the
first one (i.e. when one moves, the other one does as well).

\subsubsection{Polygon}
\drgeniusIcon{fig15}

Create a polygon defined by n points. The user selects n+1 points
delimiting the polygon. The first and last selected points are the
same; this tells \drgenius that the selection is over. The polygon
object is not an object like the other lines; it is not possible to
place a point on it or to construct an intersection between a
polygon and another line object.

\xname{transformation_tools}
\subsection{Transformation Tools}

\subsubsection{Parallel Line}
\drgeniusIcon{fig16}

Create a line passing through a point and parallel to a "direction"
(another line or line segment moving in a "direction"). The user
selects a point and a direction (i.e. a straight line, a ray, a
segment or a vector).

\subsubsection{Perpendicular Line}
\drgeniusIcon{fig17}

Create a line perpendicular to a direction and passing through a
point. The user selects a point and a direction (i.e. straight line, a
ray, a segment or a vector).

\subsubsection{Axial symmetry}
\drgeniusIcon{fig18}

Create the image of an object by an axial symmetry. The user selects
an object to transform and the axis of symmetry (a straight line).
When the user wants to construct the image of a straight line, s/he
must first select it with the mouse; the first selected line is the
line that which will be transformed.

\subsubsection{Central Symmetry}
\drgeniusIcon{fig19}

Create the image of an object by a central symmetry. The user selects
the object to transform and the centre of its symmetry (a point). When
the user wants to construct the image of a point, s/he must first
select it (with the mouse). The transformation will then be performed
on this point.

\subsubsection{Translation}
\drgeniusIcon{fig20}

Create the image of an object by translation. The user selects the
object to transform and the vector of translation. When the user wants
to construct the image of a vector, s/he must select it (with the
mouse). The transformation will then be performed on this vector.

\subsubsection{Rotation}
\drgeniusIcon{fig21}

Create the image of an object by a rotation. The user selects the
point to transform, the centre and the angle of the rotation. When the
user wants to create the image of a point, s/he must first select it
(with the mouse). The transformation will then be performed on this
selected point.

The angle can be selected from different type of value:

\begin{itemize}
\item \textbf{numeric value}: in this case the angle is expressed in
  radians. Examples of numeric values are: free value, distance between
  two points, a segment length, a coordinate, a value returned by a
  Dr.~Genius Guile Script, etc.;
\item \textbf{a geometric angle formed by three points}: in this case
  the angle is expressed in degrees. Be aware that in this case the
  angle can only fall within [0~;~180];
  \item \textbf{an oriented angle formed by two vectors}: in this case
  the angle is expressed in degrees between [0~;~360[.
\end{itemize}

\subsubsection{Scale}
\drgeniusIcon{fig22}

Create the image of a an object by a scale transformation
(homothetie). The user selects the point to transform, the centre and
the factor (i.e. a number). When the user wants to create an image of
a point, s/he must first select it; the transformation will be
performed on this selected point.

\xname{numeric_tools}
\subsection{Numeric Tools}

\subsubsection{Distance, Length \& Number}
\drgeniusIcon{fig23}

Create a numeric value. The numeric value can be computed or user-
edited depending on the user's selections:
\begin{enumerate}
 \item two points: distance between these two points;
 \item a segment: the length of this segment;
 \item a vector: the vector's magnitude;
 \item a circle: the length of the circle;
 \item an arc circle: the length of the arc;
 \item a straight line: the slope of the line;
 \item a straight line and a point: the distance between the line and
 the point;
 \item a click on the background permits the user to enter a new value
 (i.e. a free value).
\end{enumerate}

\subsubsection{Angle}
\drgeniusIcon{fig24}

Create an angle defined by three point or two vectors. In the first
case, the resulting angle is non oriented (i.e. a geometric angle in
the range [0~;~180]. In the second case, the angle is oriented and it
is within the range ]-180~;~180].

\subsubsection{Coordinates}
\drgeniusIcon{fig25}

Create the coordinates of a point or a vector. This tool creates both
the abscissa and the coordinate.

\subsubsection{\drgenius Guile Script}
\drgeniusIcon{fig49}

Create a \drgenius Guile Script. The script receives input of n
objects. It eventually returns a number, printed in the figure. A
script can be used for its side effects or for its returned value. The
\drgenius Guile Scripts are covered in details in the \link{Chapter
Advanced Features}[ Chapter \Ref]{advancedFeatures} and exactly at
\link{Section Script}[ Section \Ref, page \Pageref]{script}.


\xname{macroconstruction_tools}
\subsection{Macro-Construction Tools}

\subsubsection{Setting a Macro-Construction}
\drgeniusIcon{fig26}

Extract a construction sequence of a figure and turn it as a
macro-construction. 

\subsubsection{Executing a Macro-Construction}
\drgeniusIcon{fig27}

Execute (i.e. ``run'' or ``play'') a pre-built
macro-construction. The macro-construction can be either newly created 
or loaded from file. 

\drgeniusNote{Macro-Constructions are exposed in \link{Section
Macro-Construction}[Section \Ref, page \Pageref]{macroConstruction}.}

\xname{other_tools}
\section{Other functions}

\subsection{Moving figure}

Figure can be moved by pressing CONTROL key and button 1 of the mouse.

\subsection{Moving object}
\drgeniusIcon{fig28}

An object can be moved by draggin it. The figure is
then recomputed according to its properties.  Almost every type of
geometric object can be moved. When necessary, \drgenius performs a
reverse dragging. For example, this occurs when the user moves a line
defined by two points, \drgenius will move the two points according to
the desired line position.


\subsection{Deleting Objects}
\drgeniusIcon{fig30}

An object on the figure can be deleted when activating this menu.
Eventually, the user can undelete using the undo function from the
toolbar or application menu. By default the undo level is set to 10
but the user can adjust this value from the preference dialog.


\xname{editing_object_styles}
\subsection{Editing Object Styles}
\drgeniusIcon{fig29}

Each object owns such style attributes as colour, thickness, label,
size or shape. Additionally, it is possible to hide an object
temporarily without deleting it. For example, it can be useful to hide
intermediate constructions without deleting them.  All these
attributes can be adjusted from a dialog activated when the user
selects an object in the figure.

The point style dialog concerns any kind of point object. From it, it
is possible to adjust its color, shape, size, name and visibility.
\drgeniusFigure{The style dialog for point object}{fig31}

The line style dialog concerns straight line, ray, segment, vector,
circle, arc circle, locus. From it, it is possible to adjust the
color, style, name and visibility.  

\drgeniusFigure{The style dialog for line object}{fig32}

The numeric \& polygon style dialog concerns all kind of values (user
edited, computed by user's \drgenius Guile Script or representing
geometric measure) and polygon shape.  

\drgeniusFigure{The style dialog for numeric \& polygon objects}{fig33}

\subsection{Editing Object Properties}
\drgeniusIcon{fig52}
Some objects own properties adjustable by the user. When the user
clicks on such objects, an appropriate dialog appears. Actually the
following objects own user adjustable properties:
\begin{enumerate}
 \item Free point on the area: abscissa and coordinate can be
 edited;\\
 \drgeniusFigure{Edit free point coordinates}{fig34}

 \item Free value: its value can be edited;\\
 \drgeniusFigure{Edit free value}{fig36}

 \item Script: its code can be edited.\\
 \drgeniusFigure{Edit a script}{fig35}

\end{enumerate}


\section{User's Preferences}

\xname{default_behaviour}
\subsection{Default Behaviour}
\label{default_behaviour}

\drgenius default behaviour can be customised in several ways.  To
adjust the preferences, the user goes to the menu item
\drgeniusMenu{Edit->Preferences...} to open the preferences dialog.

The dialog is composed of a notebook with two pages:
\begin{enumerate}
 \item The first page concerns global preferences:\\
 \drgeniusFigureSize{Global preferences}{fig37}{8}
 Actually, the user can only adjust the default name of a session to
 be saved on disk.
 
 \item The second page is about geometric figure preferences:\\
 \drgeniusFigureSize{Geometric figure preferences}{fig38}{8}
 From this page, a wide range of option can be adjusted:
 \begin{itemize}
 \item The number of Undo/Redo level;
 \item The default figure name to be used when a new figure is
 created. The \%d is replaced by an integer value handled
 by \drgenius, this value is incremented at each newly created figure;
 \item Several default name for single figure file and
 LaTeX/PostScript export;
 \item In the superior area of the page, another notebook allows the
 user to adjust the default object style of each figure item (geometric or
 numeric ones). The concerned styles are colour, shape, size and style.
 \end{itemize}
\end{enumerate}

\subsection{Other Preferences}

In addition to changing the default behaviour of \drgenius, the user can
change the name of a figure from the menu item
\drgeniusMenu{Edit->Rename}.

\drgeniusFigure{Renaming a figure view}{fig39}

\chapter{Advanced Features}
\label{advancedFeatures}

In this chapter we present features used to extend
the cabilities of \drgenius in various ways. 

The first is macro-construction; it allows the extraction of a
construction logic in a record. Then, this record can be repeated or
saved as a file.

The \drgenius Guile Script features are the other means for extending
\drgenius. These scripts are really figure items like the other
geometric items. They receive as input, user selected figure item
references and they return values, plugged into figures.
 
\drgenius Guile Scripts can be useful for their returned values or their
side effects; which really depends on what the user wants to achieve.

The last feature is user-interface customisability. The idea is to
give the teacher the cability to prepare geometric figure files with
tools taken from the Dr.~Genius toolbar and contextual menu. This
capability may help in achieving a desired ``pedagogical effect''

\xname{using_macro}
\section{Macro-Construction}
\label{macroConstruction}

Macro-constructions are a bit like a procedure which receives as input
figure items and then returns one or more figure items, constructed by
the macro-construction. They are built on a model defined by the user.
This means that the user has to build a construction sequence only
once. Then she can tell \drgenius that she wants this sequence to be
recorded in a macro-construction. To record a construction sequence,
\drgenius needs to know which are the initial items of the sequence
and the resulting items. Of course the resulting items must depend
\textit{only} on the initial items; otherwise Dr.~Genius would not be
able to deduce the resulting items from the initial ones.

Using its knowledge of the initial and resulting items, \drgenius
deduces the construction sequence and saves it in a
macro-construction. The user can then execute this macro-construction
simply by inputting (initial) items (they must match the type) in the
figure.  The macro-construction can then build the resulting items.

\drgeniusNote{Invisible intermediate figure items are also built by
  the macro-construction. These items are necessary to build the
  resulting items.}


To illustrate the macro-construction feature, we will use an example
in which the user wants to record the construction of  
a circle with a given center which passes through three points, also given.

\drgeniusFigureSize{Our initial figure}{fig40}{4}

Before constructing the macro-construction, the user needs to
construct the final figure; it is used as a pattern from which to build the
macro-construction.

\drgeniusFigureSize{Our figure with the resulting construction}{fig41}{4}

\subsection{Constructing a Macro-Construction}

At this point, the construction sequence is done. Now, the user needs
to tell Dr.~Genius that she wants to create a macro-construction from this
sequence. She can call the \drgeniusMenu{Construct a macro} function
from the icon \drgeniusIcon{fig26} or from the contextual menu in the
figure view.

From the wizard dialog, the user selects the input \& output
parameters, the name and description of the macro-construction.

\drgeniusFigureSize{The 1st page of the wizard dialog to construct a
  macro-construction}{fig42}{6}

The second page of the dialog is used to select the input parameters. In
our example, these are the three initial points. The user just needs to
go to this second page and she can select the three points in the
figure. The selected items will blink.

\drgeniusFigureSize{The 2nd page, the three points are already
selected}{fig43}{6}

From the 3rd page, she selects the output parameters. In our
example, we want the circle and its centre as the result of the
macro-construction. She proceeds as in the case of the input
parameters to select them.

\drgeniusFigureSize{The 3rd page, the circle and its centre are
  already selected}{fig44}{6}

In the 4th page, the user enters the name and the description of the
macro-construction. This information is displayed when the user
executes a macro-construction, so it helps to distinguish
macro-construction between them.

\drgeniusFigureSize{The 4th page, the name and description of the
macro-construction}{fig45}{6}

From the last page of the wizard dialog (the 5th one), the user can
terminate the creation by pressing the \texttt{Finish}
button. Alternatively, she can return to previous pages to adjust
the macro-construction parameters.

\drgeniusNote{If the input \& output parameters selections do not
  match (\drgenius cannot extract the logic of the construction), the
  macro-construction cannot be built. In such a case, the user needs to
  reconsider the input and output parameters selection. She can go
  back to the 2nd and 3rd page of the wizard dialog to adjust her
  choices.}


At this point the macro-construction is built and recorded in
\drgenius. In the next section, we will see how to use this
macro-construction.

\subsection{Executing a Macro-Construction}

To execute a macro-construction, the user calls the
\drgeniusMenu{Execute pre-build macro} function from the icon
\drgeniusIcon{fig27} or from the contextual menu in the figure view.

From the wizard dialog, the user selects the macro-construction. In
the 2nd page, she selects the macro-construction from the list in the
top area of the dialog. Once a macro-construction is selected, she can
directly click on the input parameters in the figure. As soon as all
the necessary input parameters are selected, the macro-construction is
executed and the final parameters appear.

\drgeniusFigureSize{The user selects the input parameters directly in
  the figure}{fig46}{6}

In our example, the macro-construction needs three input parameters
(three points) and it builds a point and a circle. To execute our
macro-construction, we need a figure with a least three points.

\drgeniusFigureSize{A figure with three points}{fig47}{4}

Once our macro-construction is applied to these three points, we get 
the desired circle and its centre.

\drgeniusFigureSize{The resulting figure with the circle and its centre}{fig48}{4}

\xname{drgenius_guile_script}
\section{\drgenius Guile Script}
\label{script}

\drgenius is Guile aware. This means it is possible to execute Guile
scripts within \drgenius. But what is Guile? From the Guile manual:

\begin{quote}{\em
    Guile is an interpreter for the Scheme programming language,
    packaged for use in a wide variety of environments.}
\end{quote}

The following citation describes accurately how is used Guile within
\drgenius:

\begin{quote}{\em
    Like a shell, Guile can run interactively, reading expressions
    from the user, evaluating them, and displaying the results, or as
    a script interpreter, reading and executing Scheme code from a
    file.  However, Guile is also packaged as an object library,
    allowing other applications to easily incorporate a complete
    Scheme interpreter.  An application can use Guile as an extension
    language, a clean and powerful configuration language, or as
    multi-purpose "glue", connecting primitives provided by the
    application.}
\end{quote}

In \drgenius, an API is available from the Guile interpretor. This API
is a set of hooks in the geometric engine. Therefore the user can
write scripts to manipulate figure items (geometric and numeric). Also,
as scripts are figure items like any other, they do not need to be
saved as separate files. They are saved in the figure file.  In the
following, we will use the DGS acronym for \drgenius Guile Script.

\subsection{Creating a DGS}

\subsection{More about DGS}

\section{User interface customisation}
\label{user_interface-custome}


\chapter{Saving and Loading Constructions}

Constructions can be saved in two ways. One construction per file or a
construction set per file (i.e. a Dr.~Genius session).

\section{Saving one construction}

From the menu \drgeniusMenu{File->Save} or \drgeniusMenu{File->Save
  As...}, a file can be saved containing the figure under the active
  view. 
\drgeniusNote{Dr.~Genius can work with several figures at the same
  time. The user can switch from one figure to another one by clicking on
  the corresponding tabs.}

With the second menu, the user can change the name of the saved
  document.

\drgeniusNote{The default file name proposed to the user by Dr.~Genius
  can be changed from the \drgeniusMenu{Edit->Preferences...} menu.
For more information see
\link{Section Defaut Behaviour}[Section \Ref, page \Pageref]{default_behaviour}.}

\section{Saving a Session}

A session is a set of Dr.~Genius data the user wants to save in
one shot in a file. It allows teacher to organise a set of data
(figure, macro-construction, note) in one file, so it is more
convenient to re-open such data.

From the \drgeniusMenu{File->Save Multiple} menu, the user can display
 the session dialog.

\drgeniusFigureSize{The Dr.~Genius session dialog}{fig51}{8}

The list of all active data is saved in this file and  presented in a
table. The first colon represents the type of data contained in
Dr.~Genius. The second one represents the name of the data.

\drgeniusNote{Actually, a session can contain three kinds of data: 2D
  interactive figure, macro-construction and text.}

Then users can decide to select one by one the data to save from the
list. Then she can press the \drgeniusMenu{Save Session} button.
Alternatively, she can save all the data by pressing the
\drgeniusMenu{Save All} button.

\drgeniusNote{\drgeniusMenu{File->Save Multiple} menu is the only way
  to save macro-constrcutions to a file.}

\section{Loading Constructions}

\chapter{Cook Book}

\W \begin{iftex} 
\appendix 
\listoffigures 
\W \end{iftex}


\end{document}
