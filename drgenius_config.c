/*
 *  Dr Genius interactive geometry software
 * (C) 2001,2002 Copyright FSF 
 * Author: Hilaire Fernandes  2001
 * 
 * 
 * 
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glade/glade.h>
#include "drgenius_config.h"

extern GnomeHelpMenuEntry drgeniusHelp[];


static GnomePropertyBox *propertyBox;

static GladeXML *xmlPropertyBox;
static gint dialogPointColor;
static gint dialogPointShape;
static gint dialogPointSize;
static gint dialogPolygonColor;
static gint dialogSegmentColor;
static gint dialogSegmentStyle;
static gint dialogHalflineColor;
static gint dialogHalflineStyle;
static gint dialogLineColor;
static gint dialogLineStyle;
static gint dialogCircleColor;
static gint dialogCircleStyle;
static gint dialogArccircleColor;
static gint dialogArccircleStyle;
static gint dialogVectorColor;
static gint dialogVectorStyle;
static gint dialogLocusColor;
static gint dialogLocusStyle;
static gint dialogAngleColor;
static gint dialogAngleStyle;
static gint dialogScalarColor;

/* 
   Static callback for the property dialog 
*/
/* on point */
static void on_pointColor_toggled (GtkToggleButton *b, gpointer colorIndex);
static void on_pointShape_toggled (GtkToggleButton *b, gpointer shapeIndex);
static void on_pointSize_toggled (GtkToggleButton *b, gpointer sizeIndex);
/* polygon */
static void on_polygonColor_toggled (GtkToggleButton *b, gpointer colorIndex);
/* on segment */
static void on_segmentColor_toggled (GtkToggleButton *b, gpointer colorIndex);
static void on_segmentStyle_toggled (GtkToggleButton *b, gpointer styleIndex);
/* on halfline */
static void on_halflineColor_toggled (GtkToggleButton *b, gpointer colorIndex);
static void on_halflineStyle_toggled (GtkToggleButton *b, gpointer styleIndex);
/* on line */
static void on_lineColor_toggled (GtkToggleButton *b, gpointer colorIndex);
static void on_lineStyle_toggled (GtkToggleButton *b, gpointer styleIndex);
/* on vector */
static void on_vectorColor_toggled (GtkToggleButton *b, gpointer colorIndex);
static void on_vectorStyle_toggled (GtkToggleButton *b, gpointer styleIndex);
/* on circle */
static void on_circleColor_toggled (GtkToggleButton *b, gpointer colorIndex);
static void on_circleStyle_toggled (GtkToggleButton *b, gpointer styleIndex);
/* on arc circle */
static void on_arccircleColor_toggled (GtkToggleButton *b, gpointer colorIndex);
static void on_arccircleStyle_toggled (GtkToggleButton *b, gpointer styleIndex);
/* on locus */
static void on_locusColor_toggled (GtkToggleButton *b, gpointer colorIndex);
static void on_locusStyle_toggled (GtkToggleButton *b, gpointer styleIndex);
/* on angle */
static void on_angleColor_toggled (GtkToggleButton *b, gpointer colorIndex);
static void on_angleStyle_toggled (GtkToggleButton *b, gpointer styleIndex);
/* on scalar */
static void on_scalarColor_toggled (GtkToggleButton *b, gpointer colorIndex);


/* We record the state of the property dialog until the user
   decide to activate or not */

/* 
 *
 *  Global preferences 
 *
 */ 

gchar *globalSessionFileName;

/* 
 *
 * Geometric figure preferences
 *
 */

/* Point - Type define in drgeo/drgeo_drgeoStyle.h */
gint pointColor;
gint pointShape;
gint pointSize;

/* Polygon */
gint polygonColor;

/* Segment - Type define in drgeo/drgeo_drgeoStyle.h */
gint segmentColor;
gint segmentStyle;

/* half-line - Type define in drgeo/drgeo_drgeoStyle.h */
gint halflineColor;
gint halflineStyle;

/* Line - Type define in drgeo/drgeo_drgeoStyle.h */
gint lineColor;
gint lineStyle;

/* Vector - Type define in drgeo/drgeo_drgeoStyle.h */
gint vectorColor;
gint vectorStyle;

/* Circle - Type define in drgeo/drgeo_drgeoStyle.h */
gint circleColor;
gint circleStyle;

/* Arc circle - Type define in drgeo/drgeo_drgeoStyle.h */
gint arccircleColor;
gint arccircleStyle;

/* Locus - Type define in drgeo/drgeo_drgeoStyle.h */
gint locusColor;
gint locusStyle;

/* Angle - Type define in drgeo/drgeo_drgeoStyle.h */
gint angleColor;
gint angleStyle;

/* Scalar - Type define in drgeo/drgeo_drgeoStyle.h */
gint scalarColor;


/* Global */
gint drgeoUndo;
gchar *drgeoFigureName = NULL;
gchar *drgeoFileName = NULL;
gchar *drgeoLatexFileName = NULL;
gchar *drgeoPostscriptFileName = NULL;


static void connectWithData (GladeXML *tree, gchar *widgetName, 
			     GtkSignalFunc f, gint data)
{
	GtkWidget *widget;

	widget = glade_xml_get_widget (tree, widgetName); 
	if (widget == NULL)
	  g_print ("Cannot find widget %s\n",widgetName);
	else
	  gtk_signal_connect (GTK_OBJECT (widget),
			      "toggled",
			      f,
			      GINT_TO_POINTER (data));
}

void loadUserPreferences ()
/* Load User preference from the config file */
{
	g_free (globalSessionFileName);
	globalSessionFileName = gnome_config_get_string 
		("drgenius/Global/SessionFileName=session.drgenius");
	
	pointColor = gnome_config_get_int ("drgenius/DrGeo/PointColor=9");
	pointShape = gnome_config_get_int ("drgenius/DrGeo/PointShape=1");
	pointSize = gnome_config_get_int ("drgenius/DrGeo/PointSize=1");

	polygonColor = gnome_config_get_int ("drgenius/DrGeo/PolygonColor=7");


	segmentColor = gnome_config_get_int ("drgenius/DrGeo/SegmentColor=0");
	segmentStyle = gnome_config_get_int ("drgenius/DrGeo/SegmentStyle=1");

	halflineColor = gnome_config_get_int ("drgenius/DrGeo/HalfLineColor=0");
	halflineStyle = gnome_config_get_int ("drgenius/DrGeo/HalfLineStyle=1");

	lineColor = gnome_config_get_int ("drgenius/DrGeo/LineColor=0");
	lineStyle = gnome_config_get_int ("drgenius/DrGeo/LineStyle=1");

	vectorColor = gnome_config_get_int ("drgenius/DrGeo/VectorColor=0");
	vectorStyle = gnome_config_get_int ("drgenius/DrGeo/VectprStyle=1");

	circleColor = gnome_config_get_int ("drgenius/DrGeo/CircleColor=0");
	circleStyle = gnome_config_get_int ("drgenius/DrGeo/CircleStyle=1");

	arccircleColor = gnome_config_get_int ("drgenius/DrGeo/ArcCircleColor=0");
	arccircleStyle = gnome_config_get_int ("drgenius/DrGeo/ArcCircleStyle=1");

	locusColor = gnome_config_get_int ("drgenius/DrGeo/LocusColor=0");
	locusStyle = gnome_config_get_int ("drgenius/DrGeo/LocusStyle=1");

	angleColor = gnome_config_get_int ("drgenius/DrGeo/AngleColor=0");
	angleStyle = gnome_config_get_int ("drgenius/DrGeo/AngleStyle=1");

	scalarColor = gnome_config_get_int ("drgenius/DrGeo/ScalarColor=0");


	drgeoUndo = gnome_config_get_int ("drgenius/DrGeo/UndoLevel=10");
	g_free(drgeoFigureName);
	drgeoFigureName = gnome_config_get_string 
		("drgenius/DrGeo/FigureName=Untitled Figure %d");
	g_free(drgeoFileName);
	drgeoFileName = gnome_config_get_string 
		("drgenius/DrGeo/FileName=NoName.drgenius");
	g_free(drgeoLatexFileName);
	drgeoLatexFileName = gnome_config_get_string 
		("drgenius/DrGeo/LatexFileName=NoName.tex");
	g_free(drgeoPostscriptFileName);
	drgeoPostscriptFileName = gnome_config_get_string 
		("drgenius/DrGeo/PostscriptFileName=NoName.eps");	
}

void saveUserPreferences ()
{
	gnome_config_set_string 
		("drgenius/Global/SessionFileName", globalSessionFileName);
	
	/* point */
	gnome_config_set_int ("drgenius/DrGeo/PointColor",pointColor);
	gnome_config_set_int ("drgenius/DrGeo/PointShape",pointShape);
	gnome_config_set_int ("drgenius/DrGeo/PointSize",pointSize);
	/* polygon */
	gnome_config_set_int ("drgenius/DrGeo/PolygonColor",polygonColor);
	/* segment */
	gnome_config_set_int ("drgenius/DrGeo/SegmentColor",segmentColor);
	gnome_config_set_int ("drgenius/DrGeo/SegmentStyle",segmentStyle);
	/* halfLine */
	gnome_config_set_int ("drgenius/DrGeo/HalfLineColor",halflineColor);
	gnome_config_set_int ("drgenius/DrGeo/HalfLineStyle",halflineStyle);
	/* line */
	gnome_config_set_int ("drgenius/DrGeo/LineColor",lineColor);
	gnome_config_set_int ("drgenius/DrGeo/LineStyle",lineStyle);
	/* vector */
	gnome_config_set_int ("drgenius/DrGeo/VectorColor",vectorColor);
	gnome_config_set_int ("drgenius/DrGeo/VectorStyle",vectorStyle);
	/* circle */
	gnome_config_set_int ("drgenius/DrGeo/CircleColor",circleColor);
	gnome_config_set_int ("drgenius/DrGeo/CircleStyle",circleStyle);
	/* arcCircle */
	gnome_config_set_int ("drgenius/DrGeo/ArcCircleColor",arccircleColor);
	gnome_config_set_int ("drgenius/DrGeo/ArcCircleStyle",arccircleStyle);
	/* locus */
	gnome_config_set_int ("drgenius/DrGeo/LocusColor",locusColor);
	gnome_config_set_int ("drgenius/DrGeo/LocusStyle",locusStyle);
	/* angle */
	gnome_config_set_int ("drgenius/DrGeo/AngleColor",angleColor);
	gnome_config_set_int ("drgenius/DrGeo/AngleStyle",angleStyle);
	/* scalar */
	gnome_config_set_int ("drgenius/DrGeo/ScalarColor",scalarColor);

	gnome_config_set_int ("drgenius/DrGeo/UndoLevel",drgeoUndo);

	gnome_config_set_string 
		("drgenius/DrGeo/FigureName",drgeoFigureName);
	gnome_config_set_string 
		("drgenius/DrGeo/FileName",drgeoFileName);
	gnome_config_set_string 
		("drgenius/DrGeo/LatexFileName",drgeoLatexFileName);
	gnome_config_set_string 
		("drgenius/DrGeo/PostscriptFileName",drgeoPostscriptFileName);
	gnome_config_sync ();
}

void updateDialogFromUserPreferences (GtkObject *d)
{
	GtkWidget *w;
	
	w = glade_xml_get_widget (xmlPropertyBox, "globalSessionFileNameEntry"); 
	gtk_entry_set_text (GTK_ENTRY (w), globalSessionFileName);
	
	/* point */
	dialogPointColor = pointColor;
	switch (pointColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "pointBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "pointDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "pointGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "pointWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "pointDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "pointGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "pointDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "pointBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "pointBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "pointRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "pointOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "pointYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	dialogPointShape = pointShape;
	switch (pointShape)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "pointRound"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "pointCross"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "pointSquare"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "pointRoundEmpty"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "pointSquareEmpty"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, point shape out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	dialogPointSize = pointSize;
	switch (pointSize)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "pointSmall"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "pointNormal"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "pointLarge"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, point size out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	/* polygon */
	dialogPolygonColor = polygonColor;
	switch (polygonColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "polygonYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	/* segment */
	dialogSegmentColor = segmentColor;
	switch (segmentColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, segment color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	dialogSegmentStyle = segmentStyle;
	switch (segmentStyle)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentDashed"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentNormal"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "segmentLarge"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, segment style out of range\n");	
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	/* halfLine */
	dialogHalflineColor = halflineColor;
	switch (halflineColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, halfline color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	dialogHalflineStyle = halflineStyle;
	switch (halflineStyle)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineDashed"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineNormal"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "halflineLarge"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, halfline style out of range\n");	
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);
	/* line */
	dialogLineColor = lineColor;
	switch (lineColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "lineBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "lineDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "lineGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "lineWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "lineDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "lineGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "lineDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "lineBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "lineBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "lineRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "lineOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "lineYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, line color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	dialogLineStyle = lineStyle;
	switch (lineStyle)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "lineDashed"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "lineNormal"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "lineLarge"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, line style out of range\n");	
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);
	/* vector */
	dialogVectorColor = vectorColor;
	switch (vectorColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, vector color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	dialogVectorStyle = vectorStyle;
	switch (vectorStyle)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorDashed"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorNormal"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "vectorLarge"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, vector style out of range\n");	
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);
	/* circle */
	dialogCircleColor = circleColor;
	switch (circleColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "circleBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "circleDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "circleGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "circleWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "circleDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "circleGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "circleDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "circleBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "circleBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "circleRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "circleOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "circleYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, circle color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	dialogCircleStyle = circleStyle;
	switch (circleStyle)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "circleDashed"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "circleNormal"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "circleLarge"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, circle style out of range\n");	
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);
	/* arcCircle */
	dialogArccircleColor = arccircleColor;
	switch (arccircleColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, arccircle color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	dialogArccircleStyle = arccircleStyle;
	switch (arccircleStyle)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleDashed"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleNormal"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "arccircleLarge"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, arccircle style out of range\n");	
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);
	/* locus */
	dialogLocusColor = locusColor;
	switch (locusColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "locusBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "locusDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "locusGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "locusWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "locusDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "locusGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "locusDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "locusBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "locusBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "locusRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "locusOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "locusYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, locus color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	dialogLocusStyle = locusStyle;
	switch (locusStyle)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "locusDashed"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "locusNormal"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "locusLarge"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, locus style out of range\n");	
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);
	/* angle */
	dialogAngleColor = angleColor;
	switch (angleColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "angleBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "angleDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "angleGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "angleWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "angleDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "angleGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "angleDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "angleBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "angleBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "angleRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "angleOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "angleYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, angle color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);

	dialogAngleStyle = angleStyle;
	switch (angleStyle)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "angleDashed"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "angleNormal"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "angleLarge"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, angle style out of range\n");	
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);
	/* scalar */
	dialogScalarColor = scalarColor;
	switch (scalarColor)
	{
	case 0:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarBlack"); 
		break;
	case 1:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarDarkgrey"); 
		break;
	case 2:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarGrey"); 
		break;
	case 3:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarWhite"); 
		break;
	case 4:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarDarkgreen"); 
		break;
	case 5:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarGreen"); 
		break;
	case 6:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarDarkblue"); 
		break;
	case 7:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarBlue"); 
		break;
	case 8:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarBordeaux"); 
		break;
	case 9:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarRed"); 
		break;
	case 10:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarOrange"); 
		break;
	case 11:
		w = glade_xml_get_widget (xmlPropertyBox, "scalarYellow"); 
		break;
	default:
		g_print ("drgenius_config::updateUserPreferencesFromDialog, scalar color out of range\n");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(w), TRUE);


	w = glade_xml_get_widget (xmlPropertyBox, "drgeoUndoSpinbutton"); 
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (w), (gfloat) drgeoUndo);

	w = glade_xml_get_widget (xmlPropertyBox, "drgeoFigureNameEntry"); 
	gtk_entry_set_text (GTK_ENTRY (w), drgeoFigureName);

	w = glade_xml_get_widget (xmlPropertyBox, "drgeoFileNameEntry"); 
	gtk_entry_set_text (GTK_ENTRY (w), drgeoFileName);

	w = glade_xml_get_widget (xmlPropertyBox, "drgeoLatexFileNameEntry"); 
	gtk_entry_set_text (GTK_ENTRY (w), drgeoLatexFileName);

	w = glade_xml_get_widget (xmlPropertyBox, "drgeoPostscriptFileNameEntry"); 
	gtk_entry_set_text (GTK_ENTRY (w), drgeoPostscriptFileName);
}

void updateUserPreferencesFromDialog (GtkObject *d)
{
	GtkWidget *w;
	
	w = glade_xml_get_widget (xmlPropertyBox, "globalSessionFileNameEntry"); 
	g_free (globalSessionFileName);
	globalSessionFileName = g_strdup (gtk_entry_get_text (GTK_ENTRY (w)));
	
	/* point */
	pointColor = dialogPointColor;
	pointShape = dialogPointShape;
	pointSize = dialogPointSize;

	w = glade_xml_get_widget (xmlPropertyBox, "drgeoUndoSpinbutton"); 
	drgeoUndo = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (w));

	/* polygon */
	polygonColor = dialogPolygonColor;

	/* segment */
	segmentColor = dialogSegmentColor;
	segmentStyle = dialogSegmentStyle;
	/* halfLine */
	halflineColor = dialogHalflineColor;
	halflineStyle = dialogHalflineStyle;
	/* line */
	lineColor = dialogLineColor;
	lineStyle = dialogLineStyle;
	/* vector */
	vectorColor = dialogVectorColor;
	vectorStyle = dialogVectorStyle;
	/* circle */
	circleColor = dialogCircleColor;
	circleStyle = dialogCircleStyle;
	/* arcCircle */
	arccircleColor = dialogArccircleColor;
	arccircleStyle = dialogArccircleStyle;
	/* lcous */
	locusColor = dialogLocusColor;
	locusStyle = dialogLocusStyle;
	/* angle */
	angleColor = dialogAngleColor;
	angleStyle = dialogAngleStyle;
	/* scalar */
	scalarColor = dialogScalarColor;


	w = glade_xml_get_widget (xmlPropertyBox, "drgeoFigureNameEntry"); 
	drgeoFigureName = g_strdup (gtk_entry_get_text (GTK_ENTRY (w)));

	w = glade_xml_get_widget (xmlPropertyBox, "drgeoFileNameEntry"); 
	drgeoFileName = g_strdup (gtk_entry_get_text (GTK_ENTRY (w)));

	w = glade_xml_get_widget (xmlPropertyBox, "drgeoLatexFileNameEntry"); 
	drgeoLatexFileName = g_strdup (gtk_entry_get_text (GTK_ENTRY (w)));

	w = glade_xml_get_widget (xmlPropertyBox, "drgeoPostscriptFileNameEntry"); 
	drgeoPostscriptFileName = g_strdup (gtk_entry_get_text (GTK_ENTRY (w)));      
	
	saveUserPreferences ();
}

void initPreferencesBox ()
{
	if (propertyBox == NULL)
	{
  
		xmlPropertyBox = glade_xml_new (DRGENIUS_GLADEDIR"/drgenius.glade",
						"propertybox");
		propertyBox = (GnomePropertyBox *) glade_xml_get_widget 
			(xmlPropertyBox, 
			 "propertybox");
		glade_xml_signal_autoconnect (xmlPropertyBox);
		/* Connect with userdata (LibGlade is not very helpfull there. */
		
		gtk_signal_connect (GTK_OBJECT (propertyBox), "help",
				    GTK_SIGNAL_FUNC(gnome_help_pbox_goto),
				    (gpointer) &(drgeniusHelp[1]));

		/* Point */
		connectWithData (xmlPropertyBox, "pointBlack",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 0);
		connectWithData (xmlPropertyBox, "pointDarkgrey",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 1);
		connectWithData (xmlPropertyBox, "pointGrey",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 2);
		connectWithData (xmlPropertyBox, "pointWhite",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 3);
		connectWithData (xmlPropertyBox, "pointDarkgreen",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 4);
		connectWithData (xmlPropertyBox, "pointGreen",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 5);
		connectWithData (xmlPropertyBox, "pointDarkblue",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 6);
		connectWithData (xmlPropertyBox, "pointBlue",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 7);
		connectWithData (xmlPropertyBox, "pointBordeaux",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 8);
		connectWithData (xmlPropertyBox, "pointRed",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled),  9);
		connectWithData (xmlPropertyBox, "pointOrange",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 10);
		connectWithData (xmlPropertyBox, "pointYellow",
				 GTK_SIGNAL_FUNC (on_pointColor_toggled), 11);

		connectWithData (xmlPropertyBox, "pointRound",
				 GTK_SIGNAL_FUNC (on_pointShape_toggled), 0);
		connectWithData (xmlPropertyBox, "pointSquare",
				 GTK_SIGNAL_FUNC (on_pointShape_toggled), 2);
		connectWithData (xmlPropertyBox, "pointCross",
				 GTK_SIGNAL_FUNC (on_pointShape_toggled), 1);
		connectWithData (xmlPropertyBox, "pointRoundEmpty",
				 GTK_SIGNAL_FUNC (on_pointShape_toggled), 3);
		connectWithData (xmlPropertyBox, "pointSquareEmpty",
				 GTK_SIGNAL_FUNC (on_pointShape_toggled), 4);
  
		connectWithData (xmlPropertyBox, "pointSmall",
				 GTK_SIGNAL_FUNC (on_pointSize_toggled), 0);
		connectWithData (xmlPropertyBox, "pointNormal",
				 GTK_SIGNAL_FUNC (on_pointSize_toggled), 1);
		connectWithData (xmlPropertyBox, "pointLarge",
				 GTK_SIGNAL_FUNC (on_pointSize_toggled), 2);

		/* Polygon */
		connectWithData (xmlPropertyBox, "polygonBlack",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 0);
		connectWithData (xmlPropertyBox, "polygonDarkgrey",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 1);
		connectWithData (xmlPropertyBox, "polygonGrey",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 2);
		connectWithData (xmlPropertyBox, "polygonWhite",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 3);
		connectWithData (xmlPropertyBox, "polygonDarkgreen",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 4);
		connectWithData (xmlPropertyBox, "polygonGreen",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 5);
		connectWithData (xmlPropertyBox, "polygonDarkblue",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 6);
		connectWithData (xmlPropertyBox, "polygonBlue",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 7);
		connectWithData (xmlPropertyBox, "polygonBordeaux",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 8);
		connectWithData (xmlPropertyBox, "polygonRed",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled),  9);
		connectWithData (xmlPropertyBox, "polygonOrange",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 10);
		connectWithData (xmlPropertyBox, "polygonYellow",
				 GTK_SIGNAL_FUNC (on_polygonColor_toggled), 11);

		/* Segment */
		connectWithData (xmlPropertyBox, "segmentBlack",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 0);
		connectWithData (xmlPropertyBox, "segmentDarkgrey",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 1);
		connectWithData (xmlPropertyBox, "segmentGrey",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 2);
		connectWithData (xmlPropertyBox, "segmentWhite",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 3);
		connectWithData (xmlPropertyBox, "segmentDarkgreen",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 4);
		connectWithData (xmlPropertyBox, "segmentGreen",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 5);
		connectWithData (xmlPropertyBox, "segmentDarkblue",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 6);
		connectWithData (xmlPropertyBox, "segmentBlue",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 7);
		connectWithData (xmlPropertyBox, "segmentBordeaux",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 8);
		connectWithData (xmlPropertyBox, "segmentRed",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled),  9);
		connectWithData (xmlPropertyBox, "segmentOrange",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 10);
		connectWithData (xmlPropertyBox, "segmentYellow",
				 GTK_SIGNAL_FUNC (on_segmentColor_toggled), 11);

		connectWithData (xmlPropertyBox, "segmentDashed",
				 GTK_SIGNAL_FUNC (on_segmentStyle_toggled), 0);
		connectWithData (xmlPropertyBox, "segmentNormal",
				 GTK_SIGNAL_FUNC (on_segmentStyle_toggled), 1);
		connectWithData (xmlPropertyBox, "segmentLarge",
				 GTK_SIGNAL_FUNC (on_segmentStyle_toggled), 2);
		/* Halfline */
		connectWithData (xmlPropertyBox, "halflineBlack",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 0);
		connectWithData (xmlPropertyBox, "halflineDarkgrey",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 1);
		connectWithData (xmlPropertyBox, "halflineGrey",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 2);
		connectWithData (xmlPropertyBox, "halflineWhite",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 3);
		connectWithData (xmlPropertyBox, "halflineDarkgreen",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 4);
		connectWithData (xmlPropertyBox, "halflineGreen",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 5);
		connectWithData (xmlPropertyBox, "halflineDarkblue",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 6);
		connectWithData (xmlPropertyBox, "halflineBlue",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 7);
		connectWithData (xmlPropertyBox, "halflineBordeaux",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 8);
		connectWithData (xmlPropertyBox, "halflineRed",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled),  9);
		connectWithData (xmlPropertyBox, "halflineOrange",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 10);
		connectWithData (xmlPropertyBox, "halflineYellow",
				 GTK_SIGNAL_FUNC (on_halflineColor_toggled), 11);

		connectWithData (xmlPropertyBox, "halflineDashed",
				 GTK_SIGNAL_FUNC (on_halflineStyle_toggled), 0);
		connectWithData (xmlPropertyBox, "halflineNormal",
				 GTK_SIGNAL_FUNC (on_halflineStyle_toggled), 1);
		connectWithData (xmlPropertyBox, "halflineLarge",
				 GTK_SIGNAL_FUNC (on_halflineStyle_toggled), 2);
		/* Line */
		connectWithData (xmlPropertyBox, "lineBlack",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 0);
		connectWithData (xmlPropertyBox, "lineDarkgrey",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 1);
		connectWithData (xmlPropertyBox, "lineGrey",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 2);
		connectWithData (xmlPropertyBox, "lineWhite",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 3);
		connectWithData (xmlPropertyBox, "lineDarkgreen",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 4);
		connectWithData (xmlPropertyBox, "lineGreen",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 5);
		connectWithData (xmlPropertyBox, "lineDarkblue",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 6);
		connectWithData (xmlPropertyBox, "lineBlue",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 7);
		connectWithData (xmlPropertyBox, "lineBordeaux",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 8);
		connectWithData (xmlPropertyBox, "lineRed",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled),  9);
		connectWithData (xmlPropertyBox, "lineOrange",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 10);
		connectWithData (xmlPropertyBox, "lineYellow",
				 GTK_SIGNAL_FUNC (on_lineColor_toggled), 11);

		connectWithData (xmlPropertyBox, "lineDashed",
				 GTK_SIGNAL_FUNC (on_lineStyle_toggled), 0);
		connectWithData (xmlPropertyBox, "lineNormal",
				 GTK_SIGNAL_FUNC (on_lineStyle_toggled), 1);
		connectWithData (xmlPropertyBox, "lineLarge",
				 GTK_SIGNAL_FUNC (on_lineStyle_toggled), 2);
		/* Vector */
		connectWithData (xmlPropertyBox, "vectorBlack",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 0);
		connectWithData (xmlPropertyBox, "vectorDarkgrey",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 1);
		connectWithData (xmlPropertyBox, "vectorGrey",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 2);
		connectWithData (xmlPropertyBox, "vectorWhite",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 3);
		connectWithData (xmlPropertyBox, "vectorDarkgreen",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 4);
		connectWithData (xmlPropertyBox, "vectorGreen",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 5);
		connectWithData (xmlPropertyBox, "vectorDarkblue",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 6);
		connectWithData (xmlPropertyBox, "vectorBlue",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 7);
		connectWithData (xmlPropertyBox, "vectorBordeaux",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 8);
		connectWithData (xmlPropertyBox, "vectorRed",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled),  9);
		connectWithData (xmlPropertyBox, "vectorOrange",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 10);
		connectWithData (xmlPropertyBox, "vectorYellow",
				 GTK_SIGNAL_FUNC (on_vectorColor_toggled), 11);

		connectWithData (xmlPropertyBox, "vectorDashed",
				 GTK_SIGNAL_FUNC (on_vectorStyle_toggled), 0);
		connectWithData (xmlPropertyBox, "vectorNormal",
				 GTK_SIGNAL_FUNC (on_vectorStyle_toggled), 1);
		connectWithData (xmlPropertyBox, "vectorLarge",
				 GTK_SIGNAL_FUNC (on_vectorStyle_toggled), 2);
		/* Circle */
		connectWithData (xmlPropertyBox, "circleBlack",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 0);
		connectWithData (xmlPropertyBox, "circleDarkgrey",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 1);
		connectWithData (xmlPropertyBox, "circleGrey",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 2);
		connectWithData (xmlPropertyBox, "circleWhite",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 3);
		connectWithData (xmlPropertyBox, "circleDarkgreen",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 4);
		connectWithData (xmlPropertyBox, "circleGreen",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 5);
		connectWithData (xmlPropertyBox, "circleDarkblue",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 6);
		connectWithData (xmlPropertyBox, "circleBlue",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 7);
		connectWithData (xmlPropertyBox, "circleBordeaux",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 8);
		connectWithData (xmlPropertyBox, "circleRed",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled),  9);
		connectWithData (xmlPropertyBox, "circleOrange",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 10);
		connectWithData (xmlPropertyBox, "circleYellow",
				 GTK_SIGNAL_FUNC (on_circleColor_toggled), 11);

		connectWithData (xmlPropertyBox, "circleDashed",
				 GTK_SIGNAL_FUNC (on_circleStyle_toggled), 0);
		connectWithData (xmlPropertyBox, "circleNormal",
				 GTK_SIGNAL_FUNC (on_circleStyle_toggled), 1);
		connectWithData (xmlPropertyBox, "circleLarge",
				 GTK_SIGNAL_FUNC (on_circleStyle_toggled), 2);
		/* Arc circle */
		connectWithData (xmlPropertyBox, "arccircleBlack",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 0);
		connectWithData (xmlPropertyBox, "arccircleDarkgrey",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 1);
		connectWithData (xmlPropertyBox, "arccircleGrey",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 2);
		connectWithData (xmlPropertyBox, "arccircleWhite",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 3);
		connectWithData (xmlPropertyBox, "arccircleDarkgreen",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 4);
		connectWithData (xmlPropertyBox, "arccircleGreen",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 5);
		connectWithData (xmlPropertyBox, "arccircleDarkblue",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 6);
		connectWithData (xmlPropertyBox, "arccircleBlue",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 7);
		connectWithData (xmlPropertyBox, "arccircleBordeaux",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 8);
		connectWithData (xmlPropertyBox, "arccircleRed",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled),  9);
		connectWithData (xmlPropertyBox, "arccircleOrange",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 10);
		connectWithData (xmlPropertyBox, "arccircleYellow",
				 GTK_SIGNAL_FUNC (on_arccircleColor_toggled), 11);

		connectWithData (xmlPropertyBox, "arccircleDashed",
				 GTK_SIGNAL_FUNC (on_arccircleStyle_toggled), 0);
		connectWithData (xmlPropertyBox, "arccircleNormal",
				 GTK_SIGNAL_FUNC (on_arccircleStyle_toggled), 1);
		connectWithData (xmlPropertyBox, "arccircleLarge",
				 GTK_SIGNAL_FUNC (on_arccircleStyle_toggled), 2);
		/* Locus */
		connectWithData (xmlPropertyBox, "locusBlack",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 0);
		connectWithData (xmlPropertyBox, "locusDarkgrey",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 1);
		connectWithData (xmlPropertyBox, "locusGrey",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 2);
		connectWithData (xmlPropertyBox, "locusWhite",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 3);
		connectWithData (xmlPropertyBox, "locusDarkgreen",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 4);
		connectWithData (xmlPropertyBox, "locusGreen",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 5);
		connectWithData (xmlPropertyBox, "locusDarkblue",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 6);
		connectWithData (xmlPropertyBox, "locusBlue",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 7);
		connectWithData (xmlPropertyBox, "locusBordeaux",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 8);
		connectWithData (xmlPropertyBox, "locusRed",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled),  9);
		connectWithData (xmlPropertyBox, "locusOrange",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 10);
		connectWithData (xmlPropertyBox, "locusYellow",
				 GTK_SIGNAL_FUNC (on_locusColor_toggled), 11);

		connectWithData (xmlPropertyBox, "locusDashed",
				 GTK_SIGNAL_FUNC (on_locusStyle_toggled), 0);
		connectWithData (xmlPropertyBox, "locusNormal",
				 GTK_SIGNAL_FUNC (on_locusStyle_toggled), 1);
		connectWithData (xmlPropertyBox, "locusLarge",
				 GTK_SIGNAL_FUNC (on_locusStyle_toggled), 2);
		/* Angle */
		connectWithData (xmlPropertyBox, "angleBlack",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 0);
		connectWithData (xmlPropertyBox, "angleDarkgrey",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 1);
		connectWithData (xmlPropertyBox, "angleGrey",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 2);
		connectWithData (xmlPropertyBox, "angleWhite",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 3);
		connectWithData (xmlPropertyBox, "angleDarkgreen",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 4);
		connectWithData (xmlPropertyBox, "angleGreen",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 5);
		connectWithData (xmlPropertyBox, "angleDarkblue",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 6);
		connectWithData (xmlPropertyBox, "angleBlue",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 7);
		connectWithData (xmlPropertyBox, "angleBordeaux",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 8);
		connectWithData (xmlPropertyBox, "angleRed",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled),  9);
		connectWithData (xmlPropertyBox, "angleOrange",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 10);
		connectWithData (xmlPropertyBox, "angleYellow",
				 GTK_SIGNAL_FUNC (on_angleColor_toggled), 11);

		connectWithData (xmlPropertyBox, "angleDashed",
				 GTK_SIGNAL_FUNC (on_angleStyle_toggled), 0);
		connectWithData (xmlPropertyBox, "angleNormal",
				 GTK_SIGNAL_FUNC (on_angleStyle_toggled), 1);
		connectWithData (xmlPropertyBox, "angleLarge",
				 GTK_SIGNAL_FUNC (on_angleStyle_toggled), 2);
		/* Scalar */
		connectWithData (xmlPropertyBox, "scalarBlack",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 0);
		connectWithData (xmlPropertyBox, "scalarDarkgrey",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 1);
		connectWithData (xmlPropertyBox, "scalarGrey",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 2);
		connectWithData (xmlPropertyBox, "scalarWhite",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 3);
		connectWithData (xmlPropertyBox, "scalarDarkgreen",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 4);
		connectWithData (xmlPropertyBox, "scalarGreen",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 5);
		connectWithData (xmlPropertyBox, "scalarDarkblue",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 6);
		connectWithData (xmlPropertyBox, "scalarBlue",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 7);
		connectWithData (xmlPropertyBox, "scalarBordeaux",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 8);
		connectWithData (xmlPropertyBox, "scalarRed",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled),  9);
		connectWithData (xmlPropertyBox, "scalarOrange",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 10);
		connectWithData (xmlPropertyBox, "scalarYellow",
				 GTK_SIGNAL_FUNC (on_scalarColor_toggled), 11);
		
		/* we keep the GladeXml object to retrieve widgets references */
		//gtk_object_destroy (GTK_OBJECT (xmlPropertyBox));
		updateDialogFromUserPreferences (GTK_OBJECT (propertyBox));
		gnome_property_box_set_state (propertyBox, FALSE);
	}
	gtk_widget_show_all (GTK_WIDGET (propertyBox));
}

/*
  Callback for the property box
*/
/* on point */
static void 
on_pointColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogPointColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}
static void 
on_pointShape_toggled (GtkToggleButton *b, gpointer shapeIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogPointShape = GPOINTER_TO_INT (shapeIndex);
	gnome_property_box_changed (propertyBox);
}
static void 
on_pointSize_toggled (GtkToggleButton *b, gpointer sizeIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogPointSize = GPOINTER_TO_INT (sizeIndex);
	gnome_property_box_changed (propertyBox);
}

/* on polygon */
static void 
on_polygonColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogPolygonColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}

/* on segment */
static void 
on_segmentColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogSegmentColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}
static void 
on_segmentStyle_toggled (GtkToggleButton *b, gpointer styleIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogSegmentStyle = GPOINTER_TO_INT (styleIndex);
	gnome_property_box_changed (propertyBox);
}
/* on halfline */
static void 
on_halflineColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogHalflineColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}
static void 
on_halflineStyle_toggled (GtkToggleButton *b, gpointer styleIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogHalflineStyle = GPOINTER_TO_INT (styleIndex);
	gnome_property_box_changed (propertyBox);
}
/* on line */
static void 
on_lineColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogLineColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}
static void 
on_lineStyle_toggled (GtkToggleButton *b, gpointer styleIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogLineStyle = GPOINTER_TO_INT (styleIndex);
	gnome_property_box_changed (propertyBox);
}
/* on vector */
static void 
on_vectorColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogVectorColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}
static void 
on_vectorStyle_toggled (GtkToggleButton *b, gpointer styleIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogVectorStyle = GPOINTER_TO_INT (styleIndex);
	gnome_property_box_changed (propertyBox);
}
/* on circle */
static void 
on_circleColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogCircleColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}
static void 
on_circleStyle_toggled (GtkToggleButton *b, gpointer styleIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogCircleStyle = GPOINTER_TO_INT (styleIndex);
	gnome_property_box_changed (propertyBox);
}
/* on arccircle */
static void 
on_arccircleColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogArccircleColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}
static void 
on_arccircleStyle_toggled (GtkToggleButton *b, gpointer styleIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogArccircleStyle = GPOINTER_TO_INT (styleIndex);
	gnome_property_box_changed (propertyBox);
}
/* on locus */
static void 
on_locusColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogLocusColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}
static void 
on_locusStyle_toggled (GtkToggleButton *b, gpointer styleIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogLocusStyle = GPOINTER_TO_INT (styleIndex);
	gnome_property_box_changed (propertyBox);
}
/* on angle */
static void 
on_angleColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogAngleColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}
static void 
on_angleStyle_toggled (GtkToggleButton *b, gpointer styleIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogAngleStyle = GPOINTER_TO_INT (styleIndex);
	gnome_property_box_changed (propertyBox);
}
/* on scalar */
static void 
on_scalarColor_toggled (GtkToggleButton *b, gpointer colorIndex)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b)))
		dialogScalarColor = GPOINTER_TO_INT (colorIndex);
	gnome_property_box_changed (propertyBox);
}

void on_propertyBox_changed (GtkWidget *widget, gpointer data)
{
	gnome_property_box_changed (propertyBox);
}

void on_propertyBox_apply (GtkWidget *widget, gint page, gpointer data)
{
	if (page == -1 )
		updateUserPreferencesFromDialog (GTK_OBJECT (widget));
}




